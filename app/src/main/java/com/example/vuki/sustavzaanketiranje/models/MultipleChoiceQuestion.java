package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vuki on 15.12.2015..
 */
public class MultipleChoiceQuestion implements Serializable {

    @SerializedName("survey_id")
    int surveyId;

    @SerializedName("question_id")
    int questionId;

    @SerializedName("question_type_identificator")
    public int questionTypeIdentificator=1;
    @SerializedName("question_text")
    public String questionText;
    @SerializedName("answers")
    public List<MultipleChoiceAnswer> answers = new ArrayList<>();

    public MultipleChoiceQuestion(int questionId, String questionText, List<MultipleChoiceAnswer> answers) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.answers = answers;
    }

    public int getQuestionId() {
        return questionId;
    }

    public List<MultipleChoiceAnswer> getAnswers() {
        return answers;
    }

    public MultipleChoiceQuestion() {
    }
}
