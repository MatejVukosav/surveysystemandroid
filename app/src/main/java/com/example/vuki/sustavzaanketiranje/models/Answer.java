package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 22.12.2015..
 */
public class Answer implements Serializable{
    @SerializedName("user_id")
    int userId;

    @SerializedName("id")
    int answerId;
    @SerializedName("question_id")
    int question_id;

    @SerializedName("text")
    String answer;

    boolean chosen;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public void setQuestion_id(int question_id) {
        this.question_id = question_id;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public int getAnswerId() {
        return answerId;
    }


    public Answer() {
    }
}
