package com.example.vuki.sustavzaanketiranje.network;

/**
 * Created by Vuki on 10.11.2015..
 */
public interface ApiManagerInterface {
    ApiService getService();
}
