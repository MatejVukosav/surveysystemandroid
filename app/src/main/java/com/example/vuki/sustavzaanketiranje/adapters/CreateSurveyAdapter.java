package com.example.vuki.sustavzaanketiranje.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.vuki.sustavzaanketiranje.DividerItemDecoration;
import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.activities.CreateNewSurveyActivity;
import com.example.vuki.sustavzaanketiranje.activities.EditMultipleChoiceActivity;
import com.example.vuki.sustavzaanketiranje.activities.EditSingleChoiceActivity;
import com.example.vuki.sustavzaanketiranje.activities.EditTextOnlyActivity;
import com.example.vuki.sustavzaanketiranje.activities.FilledSurveyAnswerSet;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.models.FilledSurveyRequest;
import com.example.vuki.sustavzaanketiranje.models.MultipleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.MultipleChoiceQuestion;
import com.example.vuki.sustavzaanketiranje.models.SingleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.SingleChoiceQuestion;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.SurveyCreateResponse;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyAnswer;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyQuestion;
import com.example.vuki.sustavzaanketiranje.models.UpdateSurveyResponse;
import com.example.vuki.sustavzaanketiranje.models.User;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Vuki on 14.12.2015..
 */
public class CreateSurveyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static int surveyItemIterator = 0;
    static Survey survey;
    static Context context;

    public static final int HEADER = 0;
    public static final int SINGLE_CHOICE = 1;
    public static final int MULTIPLE_CHOICE = 2;
    public static final int TEXT_ONY = 3;
    public static final int FINISH = 4;


    private static boolean READ_RIGHTS;
    private static boolean EDIT_RIGHTS;
    private static boolean FILL_RIGHTS;
    static boolean mySurveyEdit;

    public static String questionPositionInRecycler = "Question_position";


    public CreateSurveyAdapter(Survey survey, Context context, boolean READ_RIGHTS, boolean EDIT_RIGHTS, boolean FILL_RIGHTS, boolean mySurveyEdit ) {
        this.survey = survey;
        this.context = context;

        this.READ_RIGHTS = READ_RIGHTS;
        this.EDIT_RIGHTS = EDIT_RIGHTS;
        this.FILL_RIGHTS = FILL_RIGHTS;


        this.mySurveyEdit = mySurveyEdit;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;

        switch (viewType) {
            case HEADER:
                v = inflater.inflate(R.layout.create_header, parent, false);
                viewHolder = new VHHeader(v);
                break;
            case SINGLE_CHOICE:
                v = inflater.inflate(R.layout.create_survey_questions_list, parent, false);
                viewHolder = new VHSingleChoice(v);
                break;
            case MULTIPLE_CHOICE:
                v = inflater.inflate(R.layout.create_survey_questions_list, parent, false);
                viewHolder = new VHMultipleChoice(v);
                break;
            case TEXT_ONY:
                v = inflater.inflate(R.layout.create_survey_questions_list, parent, false);
                viewHolder = new VHTextOnly(v);
                break;
            case FINISH:
                v = inflater.inflate(R.layout.create_finish_survey, parent, false);
                viewHolder = new VHFinish(v);
                break;
            default:
                viewHolder = null;
        }

        return viewHolder;
    }


    @Override
    public int getItemViewType(int position) {

        if (position != 0 && position != survey.getQuestions().size() + 1) {
            Object question = survey.getQuestions().get(position - 1);

            if (question instanceof SingleChoiceQuestion) {
                return SINGLE_CHOICE;

            } else if (question instanceof MultipleChoiceQuestion) {
                return MULTIPLE_CHOICE;

            } else if (question instanceof TextOnlyQuestion) {
                return TEXT_ONY;
            }

        } else {
            if (position == 0) {
                return HEADER;
            } else if (position == survey.getQuestions().size() + 1) {
                return FINISH;
            }
        }
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        surveyItemIterator = position;

        if (READ_RIGHTS) {
            userHaveReadRights(position, holder);
        } else if (EDIT_RIGHTS) {
            userHaveEditRights(position, holder);
        } else if (FILL_RIGHTS) {
            userHaveFillRIghts(position, holder);
        }
    }

    /**
     * User have write rights. He can't change,add or remove questions and answers but can choose answers and fill text marks.
     *
     * @param position -position of item in list
     * @param holder   -holder for item in list
     */
    private void userHaveFillRIghts(final int position, RecyclerView.ViewHolder holder) {

        boolean fillRightsInThisMode = true;
        boolean editRightsInThisMode = false;

        if (position != 0 && position != survey.getQuestions().size() + 1) {
            final Object question = survey.getQuestions().get(position - 1);
            SingleQuestionAdapter adapter;

            /*
                      SINGLE CHOICE QUESTION
                 */
            if (question instanceof SingleChoiceQuestion) {
                VHSingleChoice vh = (VHSingleChoice) holder;
                vh.questionName.setFocusable(false);


                final SingleChoiceQuestion singleChoiceQuestion = (SingleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, singleChoiceQuestion.questionText));
                adapter = new SingleQuestionAdapter(singleChoiceQuestion.answers, context, R.layout.create_single_choice, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);
                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);



                /*
                        MULTIPLE CHOICE QUESTION
                 */

            } else if (question instanceof MultipleChoiceQuestion) {
                VHMultipleChoice vh = (VHMultipleChoice) holder;
                vh.questionName.setFocusable(false);
                //  vh.addAnswer.setVisibility(View.GONE);
                final MultipleChoiceQuestion mcq = (MultipleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, mcq.questionText));
                adapter = new SingleQuestionAdapter(mcq.answers, context, R.layout.create_multiple_choice, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);
                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);

                /*
                        TEXT ONLY QUESTION
                 */
            } else if (question instanceof TextOnlyQuestion) {
                VHTextOnly vh = (VHTextOnly) holder;
                vh.recyclerView.setFocusable(false);
                vh.questionName.setFocusable(false);
                final TextOnlyQuestion toq = (TextOnlyQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, toq.questionText));

                adapter = new SingleQuestionAdapter(toq.answers, context, R.layout.create_text_only, fillRightsInThisMode, editRightsInThisMode, null);

                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);
                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);
            }

        } else {
            //header
            if (surveyItemIterator == 0) {
                VHHeader vh = (VHHeader) holder;
                vh.mAuthor.setText(survey.getAuthor());
                vh.mName.setText(survey.getName());
                vh.mDescription.setText(survey.getDescription());

                showTimeAndDateButtons(false, vh.openedAtbtn, vh.closedAtbtn);

                vh.textViewOpeneddAt.setText(survey.getOpenedAt());
                vh.textViewClosedAt.setText(survey.getClosedAt());

                setFocusableForHeader(editRightsInThisMode, vh);
                //finish
            } else if (surveyItemIterator == survey.getQuestions().size() + 1) {
                VHFinish vh = (VHFinish) holder;
                vh.finishSurvey.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        fillSurveyApiCall(position-1);
                        ((Activity) context).finish();

                    }
                });

            }
        }
    }


    private void fillSurveyApiCall(final int surveyPosition) {
        setDataInMSurvey();
        Survey surveyRequest = CreateNewSurveyActivity.mSurvey;

        List<FilledSurveyAnswerSet> answerSetList = new ArrayList<>();

        for (Object o : surveyRequest.getQuestions()) {
            FilledSurveyAnswerSet filledSurveyAnswerSet = new FilledSurveyAnswerSet();

            if (o instanceof SingleChoiceQuestion) {
                SingleChoiceQuestion q = (SingleChoiceQuestion) o;
                filledSurveyAnswerSet.setQuestionId(q.getQuestionId());
                List<Integer> listOfAnswerIds = new ArrayList<>();
                for (Object a : q.getAnswers()) {
                    SingleChoiceAnswer answer = (SingleChoiceAnswer) a;
                    if (answer.isChosen()) {
                        listOfAnswerIds.add(answer.getId());
                    }
                }
                filledSurveyAnswerSet.setAnswers(listOfAnswerIds);
            } else if (o instanceof MultipleChoiceQuestion) {
                MultipleChoiceQuestion q = (MultipleChoiceQuestion) o;
                filledSurveyAnswerSet.setQuestionId(q.getQuestionId());
                List<Integer> listOfAnswerIds = new ArrayList<>();
                for (Object a : q.getAnswers()) {
                    MultipleChoiceAnswer answer = (MultipleChoiceAnswer) a;
                    if (answer.isChosen()) {
                        listOfAnswerIds.add(answer.getId());
                    }
                }
                filledSurveyAnswerSet.setAnswers(listOfAnswerIds);
            } else {
                TextOnlyQuestion q = (TextOnlyQuestion) o;
                List<String> textAnswer = new ArrayList<>();
                for (TextOnlyAnswer answer : q.answers) {
                    textAnswer.add(answer.getAnswer());
                }
                filledSurveyAnswerSet.setQuestionId(q.getQuestionId());
                filledSurveyAnswerSet.setAnswers(textAnswer);
            }
            answerSetList.add(filledSurveyAnswerSet);
        }

        int userId = ApiManager.getInstance().getUser().getUserId();
        int surveyId = survey.getSurveyID();
        FilledSurveyRequest filledSurveyRequest = new FilledSurveyRequest(surveyId, userId, answerSetList);


        Call<Void> fillSurveySaveCall = ApiManager.getInstance().getService().saveFilledSurvey(filledSurveyRequest);
        fillSurveySaveCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    NotesHelpers.toastMessage(context, "Save");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure " + t.getMessage());
            }
        });
    }


    /**
     * User have read rights. He can't change,add or remove questions and answers.
     *
     * @param position -position of item in list
     * @param holder   -holder for item in list
     */
    private void userHaveReadRights(int position, RecyclerView.ViewHolder holder) {

        boolean fillRightsInThisMode = false;
        boolean editRightsInThisMode = false;

        if (position != 0 && position != survey.getQuestions().size() + 1) {
            final Object question = survey.getQuestions().get(position - 1);

            SingleQuestionAdapter adapter;

            /*
                      SINGLE CHOICE QUESTION
                 */
            if (question instanceof SingleChoiceQuestion) {
                VHSingleChoice vh = (VHSingleChoice) holder;
                vh.questionName.setFocusable(false);

                final SingleChoiceQuestion singleChoiceQuestion = (SingleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, singleChoiceQuestion.questionText));

                adapter = new SingleQuestionAdapter(singleChoiceQuestion.answers, context, R.layout.create_single_choice, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);

                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);



                /*
                        MULTIPLE CHOICE QUESTION
                 */

            } else if (question instanceof MultipleChoiceQuestion) {
                VHMultipleChoice vh = (VHMultipleChoice) holder;
                vh.questionName.setFocusable(false);
                final MultipleChoiceQuestion mcq = (MultipleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, mcq.questionText));
                adapter = new SingleQuestionAdapter(mcq.answers, context, R.layout.create_multiple_choice, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);
                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);

                /*
                        TEXT ONLY QUESTION
                 */
            } else if (question instanceof TextOnlyQuestion) {
                VHTextOnly vh = (VHTextOnly) holder;
                vh.recyclerView.setFocusable(false);
                vh.questionName.setFocusable(false);
                final TextOnlyQuestion toq = (TextOnlyQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, toq.questionText));
                adapter = new SingleQuestionAdapter(toq.answers, context, R.layout.create_text_only, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);

                vh.editBtn.setVisibility(View.GONE);
                vh.addAnswer.setVisibility(View.GONE);
            }

        } else {

            if (surveyItemIterator == 0) {
                VHHeader vh = (VHHeader) holder;
                vh.mAuthor.setText(survey.getAuthor());
                vh.mName.setText(survey.getName());
                vh.mDescription.setText(survey.getDescription());
                setFocusableForHeader(editRightsInThisMode, vh);
                showTimeAndDateButtons(false, vh.openedAtbtn, vh.closedAtbtn);

                vh.textViewOpeneddAt.setText(survey.getOpenedAt());
                vh.textViewClosedAt.setText(survey.getClosedAt());

            } else if (surveyItemIterator == survey.getQuestions().size() + 1) {
                VHFinish vh = (VHFinish) holder;

                vh.finishSurvey.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        NotesHelpers.toastMessage(context, "Thanks for reading");
                        ((Activity) context).finish();
                    }
                });

            }
        }
    }


    /**
     * User have edit rights. He can add ,remove and change questions and answers.
     *
     * @param position - position of item in list
     * @param holder   - holder for item in list
     */

    SingleQuestionAdapter adapter;
    static String time = "";
    static String date = "";
    static int positionInRecycler;
    static int questionsPositionInRecycler;

    private void userHaveEditRights(final int position, RecyclerView.ViewHolder holder) {

        boolean fillRightsInThisMode = false; //
        boolean editRightsInThisMode = false; //hide delete answer mark
        boolean editHeader = true;

        final int lastPositionInRecycler = survey.getQuestions().size() + 1;

        if (position != 0 && position != lastPositionInRecycler) {
            NotesHelpers.logMessage("pozicija ", position + "");
            positionInRecycler = position - 1;
            final Object question = survey.getQuestions().get(positionInRecycler);
                /*
                      SINGLE CHOICE QUESTION
                 */
            if (question instanceof SingleChoiceQuestion) {
                VHSingleChoice vh = (VHSingleChoice) holder;
                vh.questionName.setFocusable(false);

                final SingleChoiceQuestion singleChoiceQuestion = (SingleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, singleChoiceQuestion.questionText));
                adapter = new SingleQuestionAdapter(singleChoiceQuestion.answers, context, R.layout.create_single_choice, fillRightsInThisMode, editRightsInThisMode, null);

                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);

                vh.editBtn.setVisibility(View.VISIBLE);
                vh.editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EditSingleChoiceActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(String.valueOf(SINGLE_CHOICE), singleChoiceQuestion);
                        bundle.putInt(questionPositionInRecycler, position - 1);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });

                vh.editBtn.setVisibility(View.VISIBLE);
                vh.addAnswer.setVisibility(View.GONE);


                /*

                        MULTIPLE CHOICE QUESTION
                 */

            } else if (question instanceof MultipleChoiceQuestion) {
                VHMultipleChoice vh = (VHMultipleChoice) holder;
                vh.questionName.setFocusable(false);

                final MultipleChoiceQuestion mcq = (MultipleChoiceQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, mcq.questionText));

                adapter = new SingleQuestionAdapter(mcq.answers, context, R.layout.create_multiple_choice, fillRightsInThisMode, editRightsInThisMode, null);

                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);

                vh.editBtn.setVisibility(View.VISIBLE);
                vh.editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EditMultipleChoiceActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(String.valueOf(MULTIPLE_CHOICE), mcq);
                        bundle.putInt(questionPositionInRecycler, position - 1);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });


                vh.editBtn.setVisibility(View.VISIBLE);
                vh.addAnswer.setVisibility(View.GONE);



                /*
                        TEXT ONLY QUESTION
                 */
            } else if (question instanceof TextOnlyQuestion) {
                VHTextOnly vh = (VHTextOnly) holder;
                vh.recyclerView.setFocusable(false);
                vh.questionName.setFocusable(false);

                final TextOnlyQuestion toq = (TextOnlyQuestion) question;
                vh.questionName.setText(setNumberAndQuestionName(position, toq.questionText));
                adapter = new SingleQuestionAdapter(toq.answers, context, R.layout.create_text_only, fillRightsInThisMode, editRightsInThisMode, null);
                if (vh.recyclerView.getLayoutManager() == null) {
                    vh.recyclerView = setQuestionLayoutParamsDinamicaly(vh, adapter, vh.recyclerView);
                }
                vh.recyclerView.setAdapter(adapter);

                vh.editBtn.setVisibility(View.VISIBLE);
                vh.editBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, EditTextOnlyActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(String.valueOf(TEXT_ONY), toq);
                        bundle.putInt(questionPositionInRecycler, position - 1);
                        intent.putExtras(bundle);
                        context.startActivity(intent);


                    }
                });

                vh.editBtn.setVisibility(View.VISIBLE);
                vh.addAnswer.setVisibility(View.GONE);

            }

        } else {

            if (surveyItemIterator == 0) {
                final VHHeader vh = (VHHeader) holder;

                vh.mAuthor.setText(survey.getAuthor());
                vh.mName.setText(survey.getName());
                vh.mDescription.setText(survey.getDescription());
                setFocusableForHeader(editHeader, vh);

                showTimeAndDateButtons(true, vh.openedAtbtn, vh.closedAtbtn);
                vh.textViewOpeneddAt.setText(survey.getOpenedAt());
                vh.textViewClosedAt.setText(survey.getClosedAt());


                vh.openedAtbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = Utils.setDialog(context, "Choose opened at: ");
                        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker1);

                        datePicker.setCalendarViewShown(false);
                        datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                                new DatePicker.OnDateChangedListener() {
                                    @Override
                                    public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                                        date = dayOfMonth + "." + monthOfYear + "." + year;
                                    }
                                });


                        final TimePicker tp = (TimePicker) dialog.findViewById(R.id.timePicker1);
                        tp.setIs24HourView(true);
                        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                time = hourOfDay + ":" + minute;

                            }
                        });

                        Button btnOk = (Button) dialog.findViewById(R.id.btn_dialog_ok);
                        Button btnCancel = (Button) dialog.findViewById(R.id.btn_dialog_cancel);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                date = Utils.prettyDate(datePicker);
                                time = Utils.prettyTime(tp);

                                //String selectedTime = time + " " + date;
                                String selectedTime = date + " " + time;
                                vh.textViewOpeneddAt.setText(selectedTime);
                                survey.setOpenedAt(selectedTime);

                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    }
                });


                vh.closedAtbtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = Utils.setDialog(context, "Choose closed at: ");
                        final DatePicker datePicker = (DatePicker) dialog.findViewById(R.id.datePicker1);

                        datePicker.setCalendarViewShown(false);
                        datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                                new DatePicker.OnDateChangedListener() {
                                    @Override
                                    public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                                        date = dayOfMonth + "." + monthOfYear + "." + year;

                                    }
                                });


                        final TimePicker tp = (TimePicker) dialog.findViewById(R.id.timePicker1);
                        tp.setIs24HourView(true);
                        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                time = hourOfDay + ":" + minute;

                            }
                        });

                        Button btnOk = (Button) dialog.findViewById(R.id.btn_dialog_ok);
                        Button btnCancel = (Button) dialog.findViewById(R.id.btn_dialog_cancel);
                        btnCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        btnOk.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                date = Utils.prettyDate(datePicker);
                                time = Utils.prettyTime(tp);

                                //String selectedTime = time + " " + date;
                                String selectedTime = date + " " + time;
                                vh.textViewClosedAt.setText(selectedTime);
                                survey.setClosedAt(selectedTime);

                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                });




                vh.mName.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if(s.length()==0){
                            survey.setName("");
                        }else {
                            survey.setName(vh.mName.getText().toString());
                        }
                    }
                });

                vh.mDescription.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        survey.setDescription(vh.mDescription.getText().toString());
                    }
                });


            } else if (surveyItemIterator == survey.getQuestions().size() + 1) {
                final VHFinish vh = (VHFinish) holder;
                /*
                On survey finish
                 */
                vh.finishSurvey.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mySurveyEdit) {
                            updateSurveyApiCall();
                        } else {
                            createNewSurveyApiCall();
                        }

                    }
                });
            }
        }
    }

    private void updateSurveyApiCall() {
        setDataInMSurvey();
        Survey surveyRequest = CreateNewSurveyActivity.mSurvey;
        User user = ApiManager.getInstance().getUser();
        surveyRequest.setAuthorId(user.getUserId());
        surveyRequest.setAuthor(user.getUsername());

        Call<UpdateSurveyResponse> surveysResponseCall = ApiManager.getInstance().getService().updateSurvey(surveyRequest);
        surveysResponseCall.enqueue(new Callback<UpdateSurveyResponse>() {
            @Override
            public void onResponse(Response<UpdateSurveyResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getResponseMessage() != null) {
                        NotesHelpers.toastMessage(context, "You have succesfully updated survey.");
                        ((Activity) context).finish();
                    } else if (response.body().getError() != null) {
                        NotesHelpers.toastMessage(context, "Error" + response.body().getError());
                    } else {
                        NotesHelpers.toastMessage(context, "Error: response body is empty");
                    }
                } else {
                    NotesHelpers.toastMessage(context, "Error" + " update response is not success");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
                NotesHelpers.toastMessage(context, "Failure " + t.getMessage());
            }
        });
    }


    private void createNewSurveyApiCall() {
        setDataInMSurvey();
        Survey surveyRequest = CreateNewSurveyActivity.mSurvey;
        boolean valid = true;
        if (TextUtils.isEmpty(surveyRequest.getName())) {
            NotesHelpers.toastMessage(context, "You must enter survey name");
            valid = false;
        }

        if (TextUtils.isEmpty(surveyRequest.getOpenedAt())) {
            NotesHelpers.toastMessage(context, "You must enter  survey opened date");
            valid = false;
        }

        if (TextUtils.isEmpty(surveyRequest.getClosedAt())) {
            NotesHelpers.toastMessage(context, "You must enter  survey closed date");
            valid = false;
        }


        if (valid) {

            Call<SurveyCreateResponse> surveysResponseCall = ApiManager.getInstance().getService().postCreateSurvey(surveyRequest);
            surveysResponseCall.enqueue(new Callback<SurveyCreateResponse>() {
                                            @Override
                                            public void onResponse(Response<SurveyCreateResponse> response, Retrofit retrofit) {
                                                if (response.isSuccess()) {
                                                    if (response.body().getResponse() != null) {
                                                        NotesHelpers.toastMessage(context, "You succesfully created survey");
                                                        ((Activity) context).finish();
                                                    } else {
                                                        if (response.body().getError() != null) {
                                                            NotesHelpers.toastMessage(context, "Error " + response.body().getError());
                                                        } else {
                                                            NotesHelpers.toastMessage(context, "Error" + "create survey body is null");
                                                        }
                                                    }
                                                } else {
                                                    NotesHelpers.toastMessage(context, "Error" + "create survey response is not success");
                                                }

                                            }

                                            @Override
                                            public void onFailure(Throwable t) {
                                                t.printStackTrace();
                                                NotesHelpers.toastMessage(context, "Failure " + t.getMessage());
                                            }
                                        }

            );
        }
    }

    private void showTimeAndDateButtons(boolean show, Button open, Button close) {
        if (show) {
            open.setClickable(show);
            open.setText("Choose opened at:");
            close.setClickable(show);
            close.setText("Choose closed at:");
        } else {
            open.setClickable(show);
            open.setText("Opened at:");
            close.setClickable(show);
            close.setText("Closed at:");
        }
    }


    private void setDataInMSurvey() {
        CreateNewSurveyActivity.mSurvey.setOpenedAt(survey.getOpenedAt());
        CreateNewSurveyActivity.mSurvey.setClosedAt(survey.getClosedAt());
        CreateNewSurveyActivity.mSurvey.setName(survey.getName());
        CreateNewSurveyActivity.mSurvey.setDescription(survey.getDescription());
    }


    private RecyclerView setQuestionLayoutParamsDinamicaly(RecyclerView.ViewHolder holder, SingleQuestionAdapter adapter, RecyclerView rh) {
        final RecyclerView recyclerView;
        if (holder instanceof VHSingleChoice) {
            recyclerView = ButterKnife.findById(rh, R.id.create_question_recycler_view);
        } else if (holder instanceof VHMultipleChoice) {
            recyclerView = ButterKnife.findById(rh, R.id.create_question_recycler_view);
        }
        //TextOnly
        else {
            recyclerView = ButterKnife.findById(rh, R.id.create_question_recycler_view);
        }

        final LinearLayoutManager layoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, null));
        return recyclerView;
        // recyclerView.setAdapter(adapter);
    }


    private void setFocusableForHeader(boolean edit, VHHeader viewHolder) {
        viewHolder.mName.setFocusable(edit);
        viewHolder.mAuthor.setFocusable(false);
        viewHolder.mDescription.setFocusable(edit);
    }

    private String setNumberAndQuestionName(int position, String text) {
        return position + ". " + text;
    }

    @Override
    public int getItemCount() {
        return survey.getQuestions().size() + 2;
    }


    public static class VHSingleChoice extends RecyclerView.ViewHolder {
        @Bind(R.id.question_name)
        EditText questionName;
        @Bind(R.id.create_question_recycler_view)
        RecyclerView recyclerView;
        @Bind(R.id.create_question_edit_img_btn)
        ImageButton editBtn;
        @Nullable
        @Bind(R.id.create_survey_header_add)
        ImageView addAnswer;


        public VHSingleChoice(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


    }

    public static class VHMultipleChoice extends RecyclerView.ViewHolder {
        @Bind(R.id.question_name)
        EditText questionName;
        @Bind(R.id.create_question_recycler_view)
        RecyclerView recyclerView;
        @Bind(R.id.create_question_edit_img_btn)
        ImageButton editBtn;
        @Nullable
        @Bind(R.id.create_survey_header_add)
        ImageView addAnswer;


        public VHMultipleChoice(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class VHTextOnly extends RecyclerView.ViewHolder {
        @Bind(R.id.question_name)
        EditText questionName;
        @Bind(R.id.create_question_recycler_view)
        RecyclerView recyclerView;

        @Bind(R.id.create_question_edit_img_btn)
        ImageButton editBtn;
        @Nullable
        @Bind(R.id.create_survey_header_add)
        ImageView addAnswer;

        public VHTextOnly(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class VHHeader extends RecyclerView.ViewHolder {
        @Bind(R.id.create_survey_name)
        EditText mName;

        @Bind(R.id.create_survey_description)
        EditText mDescription;
        @Bind(R.id.create_survey_author)
        EditText mAuthor;

        @Bind(R.id.closedAtBtn)
        Button closedAtbtn;
        @Bind(R.id.openedAtBtn)
        Button openedAtbtn;

        @Bind(R.id.textClosed)
        TextView textViewClosedAt;

        @Bind(R.id.textOpenedAt)
        TextView textViewOpeneddAt;


        public VHHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class VHFinish extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.create_finish_btn)
        Button finishSurvey;

        public VHFinish(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }


}
