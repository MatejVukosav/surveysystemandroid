package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 10.11.2015..
 */
public class User implements Serializable {


    @SerializedName("token")
    private String token;

    @SerializedName("username")
    String username;

    @SerializedName("id")
    int userId;
    @SerializedName("email")
    String email;

    @SerializedName("first_name")
    String firstname;

    @SerializedName("last_name")
    String lastName;

    @SerializedName("age")
    int age;

    @SerializedName("genre")
    String genre;
    @SerializedName("city")
    String city;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUserId() {
        return userId;
    }

    public User(String username, String email, String firstname, String lastName, int age, String genre, String city) {
        this.username = username;
        this.email = email;
        this.firstname = firstname;
        this.lastName = lastName;
        this.age = age;
        this.genre = genre;
        this.city = city;
    }

    public User() {
    }
}
