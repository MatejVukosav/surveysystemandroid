package com.example.vuki.sustavzaanketiranje.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.models.Statistic;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class StatisticActivity extends AppCompatActivity {
    public static final String STATISTIC = "statistic";

    @Bind(R.id.statistic_edit_text)
    EditText statisticView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        ButterKnife.bind(this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            Statistic statistic = (Statistic) b.getSerializable(STATISTIC);
            List<QuestionStatistic> questionStatisticList = statistic.getQuestionStatistic();
            String statisticString = "";

            for (QuestionStatistic questionStatistic : questionStatisticList) {
                statisticString = statisticString + "  << " + questionStatistic.questionText + " >>" + "\n";

                for (QuestionChoicesStatistic questionChoicesStatistic : questionStatistic.getQuestionChoicesStatisticList()) {
                    String answerText = "";
                    if (questionChoicesStatistic.getCount() == -1) {
                        if (questionChoicesStatistic.answerTexts != null) {
                            for (String s : questionChoicesStatistic.answerTexts) {
                                answerText = answerText +"      " +s + " \n";
                            }
                            statisticString = statisticString + "    ~ " + answerText + "\n";
                        }
                    } else {
                        answerText = questionChoicesStatistic.getAnswerText();
                        int count = questionChoicesStatistic.getCount();
                        statisticString = statisticString + "    ~ " + answerText + "  [ " + count + " ]" + "\n";
                    }

                }
                statisticString = statisticString + "\n";
            }

            statisticView.setText(statisticString);
        } else {
            statisticView.setText("There is no statistic available");
        }


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Statistic");
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
