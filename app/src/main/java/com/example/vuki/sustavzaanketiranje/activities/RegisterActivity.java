package com.example.vuki.sustavzaanketiranje.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.PrefsHelper;
import com.example.vuki.sustavzaanketiranje.helpers.SharedPrefsHelpers;
import com.example.vuki.sustavzaanketiranje.models.LoginResponse;
import com.example.vuki.sustavzaanketiranje.models.RegisterRequest;
import com.example.vuki.sustavzaanketiranje.models.User;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.register_username)
    EditText username;
    @Bind(R.id.register_email)
    EditText email;

    @Bind(R.id.register_pass1)
    EditText pass1;
    @Bind(R.id.register_pass2)
    EditText pass2;

    @Bind(R.id.register_first_name)
    EditText firstName;
    @Bind(R.id.register_last_name)
    EditText lastName;

    @Bind(R.id.radioGroupSex)
    RadioGroup radioGroup;

    @Bind(R.id.register_city)
    EditText city;
    @Bind(R.id.register_age)
    EditText age;

    String mUsername = "";
    String mEmail = "";
    String mFirstname = "";
    String mLastName = "";
    String mPass1 = "";
    String mPass2 = "";
    int mAge = 0;
    String mCity = "";
    String mGendre = "";

    PrefsHelper prefsHelper;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        prefsHelper = new PrefsHelper(this);
        context = this;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Registration");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.btn_register)
    public void OnRegistrationComplete() {
        boolean valid = true;
        if (username.length() > 3) {
            mUsername = username.getText().toString();
        } else {
            NotesHelpers.toastMessage(this, "Please enter correct username(min 4 characters) and try again!");
            valid = false;
        }

        if (email.length() > 2) {
            mEmail = email.getText().toString();
            if (!mEmail.contains("@")) {
                NotesHelpers.toastMessage(this, "Email must contain '@' !");
                valid = false;
            }
        } else {
            NotesHelpers.toastMessage(this, "Please enter valid email and try again!");
            valid = false;
        }

        if (pass1.length() > 3 && pass2.length() > 3) {
            if (pass1.getText().toString().equals(pass2.getText().toString())) {
                mPass1 = pass1.getText().toString();
                mPass2 = pass2.getText().toString();
            } else {
                NotesHelpers.toastMessage(this, "Passwords are not equal!");
                valid = false;
            }
        } else {
            NotesHelpers.toastMessage(this, "Please enter correct passwords (min 4 characters) and try again!");
            valid = false;
        }


        try {
            mFirstname = firstName.getText().toString();
            mLastName = lastName.getText().toString();
            mCity = city.getText().toString();
            mAge = Integer.valueOf(age.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            mAge = 0;
        } catch (NullPointerException en) {
            en.printStackTrace();
        }

        int radioBtnId = radioGroup.getCheckedRadioButtonId();
        RadioButton chosen = (RadioButton) findViewById(radioBtnId);
        mGendre = chosen.getText().toString();

        if (valid) {


            RegisterRequest registerRequest;
            try

            {
                registerRequest = new RegisterRequest(mUsername, mEmail, mPass1, mPass2, mFirstname, mLastName, mAge, mGendre, mCity);
            } catch (
                    NullPointerException e
                    )

            {
                registerRequest = new RegisterRequest();
                e.printStackTrace();
            }

            Call<LoginResponse> registerRequestCall = ApiManager.getInstance().getService().postRegister(registerRequest);
            registerRequestCall.enqueue(new Callback<LoginResponse>()

                                        {

                                            @Override
                                            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {

                                                if (response.isSuccess()) {
                                                    if (response.body().getUser() != null) {
                                                        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);

                                                        User user = response.body().getUser();
                                                        ApiManager.getInstance().setUser(user);
                                                        SharedPrefsHelpers.setToken(context, user.getToken());
                                                        String json = ApiManager.getGSON().toJson(user);
                                                        prefsHelper.putString(PrefsHelper.LOGGED_IN_USER_APPUSER_DATA, json);


                                                        NotesHelpers.toastMessage(getApplicationContext(), "You have successfully registrate.");
                                                        startActivity(intent);
                                                        finish();
                                                    } else if (response.body().getMessageError() != null) {
                                                        String message = response.body().getMessageError();
                                                        NotesHelpers.toastMessage(getApplicationContext(), message);
                                                    } else {
                                                        NotesHelpers.toastMessage(getApplicationContext(), "Oops,something wen't wrong. Server internal error");
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Throwable t) {
                                                Toast.makeText(getApplicationContext(), "Error " + t.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        }

            );
        }

    }
}

