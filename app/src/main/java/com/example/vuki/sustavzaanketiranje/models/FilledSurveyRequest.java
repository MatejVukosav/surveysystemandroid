package com.example.vuki.sustavzaanketiranje.models;

import com.example.vuki.sustavzaanketiranje.activities.FilledSurveyAnswerSet;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 8.1.2016..
 */
public class FilledSurveyRequest implements Serializable {
    @SerializedName("survey_id")
    int surveyId;

    @SerializedName("user_id")
    int userId;

    @SerializedName("answers")
    List<FilledSurveyAnswerSet> filledSurveyAnswerSetList;

    public FilledSurveyRequest(int surveyId, int userId, List<FilledSurveyAnswerSet> filledSurveyAnswerSetList) {
        this.surveyId = surveyId;
        this.userId = userId;
        this.filledSurveyAnswerSetList = filledSurveyAnswerSetList;
    }
}
