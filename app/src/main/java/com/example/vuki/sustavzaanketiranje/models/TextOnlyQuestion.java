package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vuki on 15.12.2015..
 */
public class TextOnlyQuestion implements Serializable {

    @SerializedName("question_id")
    int questionId;

    @SerializedName("question_type_identificator")
    public int questionTypeIdentificator=2;
    @SerializedName("question_text")
    public String questionText;
    @SerializedName("answers")
    public List<TextOnlyAnswer> answers = new ArrayList<>();

    public TextOnlyQuestion(int questionId, String questionText, List<TextOnlyAnswer> answers) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.answers = answers;
    }

    public int getQuestionId() {
        return questionId;
    }

    public List<TextOnlyAnswer> getAnswers() {
        return answers;
    }

    public TextOnlyQuestion() {
    }
}
