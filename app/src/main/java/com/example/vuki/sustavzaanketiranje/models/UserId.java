package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 9.1.2016..
 */
public class UserId implements Serializable {

    @SerializedName("user_id")
    int userId;

    public UserId(int userId) {
        this.userId = userId;
    }
}
