package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 22.12.2015..
 */
public class ResetPasswordRequest implements Serializable{

    @SerializedName("email")
    String email;


    public ResetPasswordRequest(String email) {
        this.email = email;
    }
}
