package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 3.12.2015..
 */
public class LoginResponse implements Serializable {

    @SerializedName("response")
    private User user;

    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }


    @SerializedName("error")
    private String messageError;

    public String getMessageError() {
        return messageError;
    }
}
