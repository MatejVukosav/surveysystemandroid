package com.example.vuki.sustavzaanketiranje.activities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 8.1.2016..
 */
public class FilledSurveyAnswerSet<T> implements Serializable {
    @SerializedName("question_id")
    int questionId;

    //Mogu biti ili id-ijevi ili tekst ako je tekstualni odgovor
    @SerializedName("answers")
    List<T> answers;

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public void setAnswers(List<T> answers) {
        this.answers = answers;
    }
}
