package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 29.12.2015..
 */
public class SurveyQuestionResponse implements Serializable {


    @SerializedName("name")
    String surveyName;

    @SerializedName("author")
    String surveyAuthor;

    @SerializedName("questions")
    List<Question> surveyQuestions;


    public List<Question> getSurveyQuestions() {
        return surveyQuestions;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public String getSurveyAuthor() {
        return surveyAuthor;
    }
}
