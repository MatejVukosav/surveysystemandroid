package com.example.vuki.sustavzaanketiranje.models;

import java.io.Serializable;

/**
 * Created by Vuki on 14.12.2015..
 */
public class TextOnlyAnswer implements Serializable {

    int id;
    String answer;

    public TextOnlyAnswer(String answer) {
        this.answer = answer;
    }

    public TextOnlyAnswer() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
