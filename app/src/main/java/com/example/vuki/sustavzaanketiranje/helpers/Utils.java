package com.example.vuki.sustavzaanketiranje.helpers;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.example.vuki.sustavzaanketiranje.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Vuki on 22.12.2015..
 */
public class Utils {

    //public static String TIME_FORMAT = "yyyy-MM-dd HH:MM";
    public static String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:SS'Z'";
    static SimpleDateFormat mFormat = new SimpleDateFormat(TIME_FORMAT, Locale.getDefault());


    ///if first before second return true,else false
    public static boolean compareTwoDates(String firstDateString, String secondDateString) {

        try {
            Date firstDate = mFormat.parse(firstDateString);
            Date secondDate = mFormat.parse(secondDateString);

            if (firstDate.before(secondDate)) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean dateBeforeNow(String firstDateString) {
        try {
            //NotesHelpers.toastMessage(App.getInstance(), firstDateString);
            Date firstDate = mFormat.parse(firstDateString);
            String now = getCurrentTime();
            Date secondDate = mFormat.parse(now);
            //TODO promjenjen datum

            if (firstDate.before(secondDate) || firstDate.equals(secondDate)) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getCustomDate(long time) {
        String selectedDate = mFormat.format(new Date(time));
        return selectedDate;
    }

    public static String getCurrentTime() {
        String selectedDate = mFormat.format(new Date());
        return selectedDate;
    }




    public static String prettyDate(DatePicker datePicker) {
        int dayOfMonth = datePicker.getDayOfMonth();
        int monthOfYear = datePicker.getMonth() + 1;  //fixed date picker month
        int year = datePicker.getYear();
        String date = year + "-" + monthOfYear + "-" + dayOfMonth;
        return date;
    }

    public static String prettyTime(TimePicker timePicker) {
        String time = "";
        try {
            int hourOfDay = timePicker.getCurrentHour();
            int minute = timePicker.getCurrentMinute();

            time = setLeadingZeroes(hourOfDay) + ":" + setLeadingZeroes(minute); //hourOfDay + ":" + minute;
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return time;
    }

    private static String setLeadingZeroes(int number) {

        if (number >= 10) {
            return String.valueOf(number);
        } else {
            return "0" + String.valueOf(number);
        }
    }

    //not working good
    private static String setZeroPadding(int mHour, int mMinute) {
        return String.format("%02d:%02d", mHour, mMinute);
    }

    public static void setProgressDialog( ProgressDialog mProgressDialog){
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
    }

    public static void dismissProgressDialog( ProgressDialog mProgressDialog){
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }


    public static Dialog setDialog(final Context context, String title) {
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.time_and_date_dialog);
        dialog.setTitle(title);
        return dialog;
    }

    public static Dialog showDeleteDialog(final Context context, String title){
        Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_layout);
        dialog.setTitle(title);
        return dialog;
    }


}
