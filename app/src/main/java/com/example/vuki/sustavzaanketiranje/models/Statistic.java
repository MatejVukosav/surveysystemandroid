package com.example.vuki.sustavzaanketiranje.models;

import com.example.vuki.sustavzaanketiranje.activities.QuestionStatistic;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 15.1.2016..
 */
public class Statistic implements Serializable {

    @SerializedName("statistic")
    List<QuestionStatistic> questionStatistic;

    public List<QuestionStatistic> getQuestionStatistic() {
        return questionStatistic;
    }
}

