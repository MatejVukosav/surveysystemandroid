package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 30.12.2015..
 */
public class UpdateSurveyResponse implements Serializable {

    @SerializedName("response")
    String responseMessage;

    @SerializedName("error")
    String error;

    public String getResponseMessage() {
        return responseMessage;
    }

    public String getError() {
        return error;
    }
}
