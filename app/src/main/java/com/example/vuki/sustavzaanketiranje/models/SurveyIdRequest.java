package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 29.12.2015..
 */
public class SurveyIdRequest implements Serializable {

    @SerializedName("id")
    int surveyId;

    public SurveyIdRequest(int surveyId) {
        this.surveyId = surveyId;
    }
}
