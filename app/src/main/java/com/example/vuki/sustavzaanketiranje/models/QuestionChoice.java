package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 8.1.2016..
 */
public class QuestionChoice implements Serializable {

    @SerializedName("id")
    int questionChoiceId;

    @SerializedName("text")
    String questionChoiceText;

    boolean chosen;

    public int getQuestionChoiceId() {
        return questionChoiceId;
    }

    public String getQuestionChoiceText() {
        return questionChoiceText;
    }

    public boolean isChosen() {
        return chosen;
    }
}
