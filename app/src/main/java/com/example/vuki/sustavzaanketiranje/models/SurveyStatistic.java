package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 15.1.2016..
 */
public class SurveyStatistic implements Serializable {
    @SerializedName("response")
    Statistic statistic;

    public Statistic getStatistic() {
        return statistic;
    }
}
