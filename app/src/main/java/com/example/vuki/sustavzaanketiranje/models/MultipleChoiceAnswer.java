package com.example.vuki.sustavzaanketiranje.models;

import java.io.Serializable;

/**
 * Created by Vuki on 14.12.2015..
 */
public class MultipleChoiceAnswer implements Serializable{

    int id;
    String answer;
    boolean chosen;

    public MultipleChoiceAnswer(String answer, boolean chosen) {
        this.answer = answer;
        this.chosen = chosen;
    }

    public MultipleChoiceAnswer() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
