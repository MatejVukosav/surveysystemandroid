package com.example.vuki.sustavzaanketiranje.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.vuki.sustavzaanketiranje.DividerItemDecoration;
import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.adapters.AvailableSurveysAdapter;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyClickListener;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.SurveyIdRequest;
import com.example.vuki.sustavzaanketiranje.models.SurveyQuestionResponse;
import com.example.vuki.sustavzaanketiranje.models.SurveysResponse;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class UserSurveysForFillActivity extends AppCompatActivity implements OnSurveyClickListener {

    @Bind(R.id.available_surveys_recycler_view)
    RecyclerView mAvailableSurveysRecyclerView;

    @OnClick(R.id.survey_opened)
    public void onOpenedClick() {
        NotesHelpers.toastMessage(context, "Survey is opened");
    }


    @OnClick(R.id.survey_editable)
    public void onEditableClick() {
        NotesHelpers.toastMessage(context, "Survey is editable");
    }

    @OnClick(R.id.survey_closed)
    public void onClosedClick() {
        NotesHelpers.toastMessage(context, "Survey is closed");
    }

    @OnClick(R.id.survey_not_opened_yet)
    public void onNotOpenedYetClick() {
        NotesHelpers.toastMessage(context, "Survey is not opened yet");
    }

    public static List<Survey> data = new ArrayList<>();
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_available_surveys);
        ButterKnife.bind(this);
        context = this;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Choose survey to fill");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApiSurveyList();
    }

    private void getApiSurveyList() {

        data=new ArrayList<>();
        final ProgressDialog progressDialog=new ProgressDialog(this);
        Utils.setProgressDialog(progressDialog);
        int userId=ApiManager.getInstance().getUser().getUserId();
        Call<SurveysResponse> surveysResponseCall = ApiManager.getInstance().getService().getSurveysForFill(userId);
        surveysResponseCall.enqueue(new Callback<SurveysResponse>() {
            @Override
            public void onResponse(Response<SurveysResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body().getSurveys() != null) {
                        try {
                            fillData(response.body().getSurveys());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                        AvailableSurveysAdapter adapter = new AvailableSurveysAdapter(getApplicationContext(), data, (OnSurveyClickListener) context,null, false, false,false);
                        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        mAvailableSurveysRecyclerView.setLayoutManager(llm);
                        mAvailableSurveysRecyclerView.setHasFixedSize(true);
                        mAvailableSurveysRecyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getApplicationContext(), R.drawable.abc_list_divider_mtrl_alpha)));
                        mAvailableSurveysRecyclerView.setAdapter(adapter);
                    }
                } else {
                    NotesHelpers.toastMessage(getApplicationContext(), "There is no surveys available");
                }
                Utils.dismissProgressDialog(progressDialog);
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(progressDialog);
                NotesHelpers.toastMessage(getApplicationContext(), "Failed: " + t.getMessage());
                fillData(new ArrayList<Survey>());
            }
        });


    }

    private void fillData(List<Survey> surveys) {
        for (Survey survey : surveys)
            data.add(survey);
    }

    private boolean READ_ONLY_RIGHTS; //if survey has closed
    private boolean FILL_RIGHTS; //of survey is open
    private boolean EDIT_RIGHTS; //admin mode

    @Override
    public void onRecyclerItemClickListener(Survey survey) {
        getQuestionsForSurvey(survey);
    }

    @Override
    public void onItemRemoved(int surveyPosition) {

    }

    @Override
    public void onItemUpdated(int surveyId) {

    }

    private void getQuestionsForSurvey(final Survey survey) {
        int id = survey.getSurveyID();

        SurveyIdRequest surveyIdRequest = new SurveyIdRequest(id);
        Call<SurveyQuestionResponse> surveyQuestionResponseCall = ApiManager.getInstance().getService().getSurveyQuestion(surveyIdRequest);
        surveyQuestionResponseCall.enqueue(new Callback<SurveyQuestionResponse>() {
            @Override
            public void onResponse(Response<SurveyQuestionResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getSurveyQuestions() != null) {
                        survey.setQuestions(response.body().getSurveyQuestions());
                        survey.setAuthor(response.body().getSurveyAuthor());

                        Intent surveyDetails = new Intent(UserSurveysForFillActivity.this, CreateNewSurveyActivity.class);
                        Bundle b = new Bundle();
                        b.putBoolean(CreateNewSurveyActivity.IS_MY_SURVEY, false);
                        //if true closed is before now
                        if (!Utils.dateBeforeNow(survey.getOpenedAt()) || Utils.dateBeforeNow(survey.getClosedAt())) {
                            b.putBoolean(CreateNewSurveyActivity.MARK_FILL_RIGHTS, false);
                        } else {
                            b.putBoolean(CreateNewSurveyActivity.MARK_FILL_RIGHTS, true);
                        }

                        b.putBoolean(CreateNewSurveyActivity.MARK_EDIT_RIGHTS, false);
                        b.putBoolean(CreateNewSurveyActivity.MARK_READ_ONLY_RIGHTS, false);
                        b.putSerializable(CreateNewSurveyActivity.MARK_SURVEY, survey);
                        surveyDetails.putExtras(b);
                        startActivity(surveyDetails);

                    } else if (response.body().getSurveyName() != null) {
                        NotesHelpers.toastMessage(context, "Survey " + response.body().getSurveyName() + " has no questions!");
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure: " + t.getMessage());
            }
        });
    }



}
