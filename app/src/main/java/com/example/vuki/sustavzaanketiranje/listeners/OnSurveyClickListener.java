package com.example.vuki.sustavzaanketiranje.listeners;

import com.example.vuki.sustavzaanketiranje.models.Survey;

/**
 * Created by Vuki on 4.12.2015..
 */
public interface OnSurveyClickListener {
    void onRecyclerItemClickListener(Survey survey);
    void onItemRemoved(int surveyPosition);
    void onItemUpdated(int surveyId);
}
