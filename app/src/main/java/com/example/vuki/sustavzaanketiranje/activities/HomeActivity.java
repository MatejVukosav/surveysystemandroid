package com.example.vuki.sustavzaanketiranje.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.SendEmailHelper;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {
    boolean mDoubleBackToExitPressedOnce = false;

    @Bind(R.id.home_navigation_view)
    NavigationView navigationView;

    @Bind(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        context=this;

        init();

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Home");
        }

        initDrawer();
        String username = "";
        if (ApiManager.getInstance().getUser() != null) {
            username = ApiManager.getInstance().getUser().getUsername();
        }
        View headerLayout = navigationView.getHeaderView(0);
        TextView user = (TextView) headerLayout.findViewById(R.id.home_navigation_header_username);
        user.setText(username);

    }

    String messageTitle;
    String receiver;


    private void init() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if (item.isChecked()) {
                    item.setChecked(false);
                }
                //drawerLayout.closeDrawers();

                switch (item.getItemId()) {
                /*    case R.id.menu_navigation_edit_profile:
                        Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                       Intent intent = new Intent(HomeActivity.this, EditProfileActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.menu_navigation_settings:
                        item.setVisible(false);
                        Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                        Intent settings = new Intent(HomeActivity.this, SettingsActivity.class);
                        startActivity(settings);
                        break;*/
                    case R.id.menu_navigation_ask_helpe:
                        String bugMessage = "";
                         messageTitle = "Ask_for_help_SZA";
                         receiver = "vuki146@gmail.com";
                        SendEmailHelper.sendEmail(context, receiver, messageTitle, bugMessage);
                       /* Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                        Intent askForHelp = new Intent(HomeActivity.this, AskForHelpActivity.class);
                        startActivity(askForHelp);*/
                        break;
                    case R.id.menu_navigation_report_bug:
                        String helpMessage = "";
                         messageTitle = "Bug_report_SZA";
                         receiver = "vuki146@gmail.com";
                        SendEmailHelper.sendEmail(context, receiver, messageTitle, helpMessage);
                      /*  Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                        Intent reportBug = new Intent(HomeActivity.this, ReportBugActivity.class);
                        startActivity(reportBug);*/
                        break;
                    case R.id.menu_navigation_log_out:
                        Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
                        Intent logOut = new Intent(HomeActivity.this, LoginActivity.class);
                        //TODO odjavit usera
                        startActivity(logOut);
                        finish();
                        break;
                }
                return true;
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_survey, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (item.getItemId() == android.R.id.home) {
                    if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                        drawerLayout.closeDrawer(GravityCompat.START);
                    } else {
                        drawerLayout.openDrawer(GravityCompat.START);
                    }
                }
                return true;
            case R.id.create_new_survey:
                createNewSurvey();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, null, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }


    private void createNewSurvey() {
        Intent createSurvey = new Intent(this, CreateNewSurveyActivity.class);
        bundle.putBoolean("CreateNewSurvey", true);
        createSurvey.putExtras(bundle);
        startActivity(createSurvey);
    }


    @OnClick(R.id.home_fill_survey)
    public void showSurveys() {
        Intent showAvailableSurveys = new Intent(HomeActivity.this, UserSurveysForFillActivity.class);
        bundle.putBoolean(String.valueOf(CreateNewSurveyActivity.FILL_RIGHTS) + CreateNewSurveyActivity.homeAdd, true);
        showAvailableSurveys.putExtras(bundle);
        startActivity(showAvailableSurveys);
    }

    @OnClick(R.id.home_all_surveys)
    public void showAllSurveys() {
        Intent showAllSurveys = new Intent(HomeActivity.this, UserShowAllSurveys.class);
        bundle.putBoolean(String.valueOf(CreateNewSurveyActivity.READ_ONLY_RIGHTS) + CreateNewSurveyActivity.homeAdd, true);
        showAllSurveys.putExtras(bundle);
        startActivity(showAllSurveys);
    }

    Bundle bundle = new Bundle();

    @OnClick(R.id.home_filled_surveys)
    public void showFilledSurveys() {
        Intent userFilledSurveys = new Intent(HomeActivity.this, UserFilledSurveysActivity.class);
        bundle.putBoolean(String.valueOf(CreateNewSurveyActivity.READ_ONLY_RIGHTS) + CreateNewSurveyActivity.homeAdd, true);
        userFilledSurveys.putExtras(bundle);
        startActivity(userFilledSurveys);
    }


    @OnClick(R.id.home_my_surveys)
    public void showMySurveys() {
        Intent userSurveys = new Intent(HomeActivity.this, UserMySurveys.class);
        bundle.putBoolean(String.valueOf(CreateNewSurveyActivity.EDIT_RIGHTS) + CreateNewSurveyActivity.homeAdd, true);
        userSurveys.putExtras(bundle);
        startActivity(userSurveys);
    }


    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        //after 2seconds it becomes false
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
