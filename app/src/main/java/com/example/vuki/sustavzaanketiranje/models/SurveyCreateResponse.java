package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 14.12.2015..
 */
public class SurveyCreateResponse implements Serializable{

    @SerializedName("response")
    private String response;

    @SerializedName("error_msg")
    private String error;

    public String getResponse() {
        return response;
    }

    public String getError() {
        return error;
    }


}
