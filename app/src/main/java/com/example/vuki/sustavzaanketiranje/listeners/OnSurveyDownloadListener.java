package com.example.vuki.sustavzaanketiranje.listeners;

/**
 * Created by Vuki on 9.1.2016..
 */
public interface OnSurveyDownloadListener {

    void onSurveyAnswersCsv(int surveyId, String surveyName);
    void onSurveyDownload(int surveyId, String surveyName);
}
