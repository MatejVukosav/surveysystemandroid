package com.example.vuki.sustavzaanketiranje.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.models.ResetPasswordRequest;
import com.example.vuki.sustavzaanketiranje.models.ResetPasswordResponse;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class ForgottenPasswordActivity extends AppCompatActivity {

    @Bind(R.id.forgotten_enter_email)
    EditText emailText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotten_password);
        ButterKnife.bind(this);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Reset password");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.forgotten_reset_password)
    public void onResetPasswordClick() {
        String email = emailText.getText().toString();
        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(email);
        Call<ResetPasswordResponse> resetPasswordResponseCall = ApiManager.getInstance().getService().postResetPassword(resetPasswordRequest);
        resetPasswordResponseCall.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Response<ResetPasswordResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if(response.body().getResponse()!=null) {
                        NotesHelpers.toastMessage(getApplicationContext(), "You have succesfully reset password.Please check your email for further steps.");
                   /* Intent intent = new Intent(ForgottenPasswordActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();*/
                    }else if(response.body().getError()!=null){
                        NotesHelpers.toastMessage(getApplicationContext(),response.body().getError() );
                    }else{
                        NotesHelpers.toastMessage(getApplicationContext(),"Error: response body is empty" );

                    }
                } else {
                    NotesHelpers.toastMessage(getApplicationContext(), "Oops..something wen't wrong. Our support-team will be notified.");
                }

            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(getApplicationContext(), "Oops..something wen't wrong. Please try again.");

            }
        });

    }
}
