package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vuki on 15.12.2015..
 */
public class SingleChoiceQuestion implements Serializable {

    @SerializedName("survey_id")
    int surveyId;

    @SerializedName("question_id")
    int questionId;

    @SerializedName("question_type_identificator")
    public int questionTypeIdentificator=0;

    @SerializedName("question_text")
    public String questionText;

    @SerializedName("answers")
    public List<SingleChoiceAnswer> answers = new ArrayList<>();

    public SingleChoiceQuestion(int questionId, String questionText, List<SingleChoiceAnswer> answers) {
        this.questionId = questionId;
        this.questionText = questionText;
        this.answers = answers;
    }

    public SingleChoiceQuestion() {
    }

    public int getQuestionId() {
        return questionId;
    }

    public List<SingleChoiceAnswer> getAnswers() {
        return answers;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public int getQuestionTypeIdentificator() {
        return questionTypeIdentificator;
    }

    public String getQuestionText() {
        return questionText;
    }
}
