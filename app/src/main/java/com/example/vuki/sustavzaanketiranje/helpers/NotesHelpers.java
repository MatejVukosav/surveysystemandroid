package com.example.vuki.sustavzaanketiranje.helpers;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by Vuki on 3.12.2015..
 */
public class NotesHelpers {

    public  static void logMessage(String TAG,String body){
        Log.d(TAG, body);
    }

    public  static void toastMessage(Context context,String body){
        Toast.makeText(context, body, Toast.LENGTH_SHORT).show();
    }

    public static void snackbarMessage(View v,CharSequence body){
        Snackbar.make(v,body,Snackbar.LENGTH_SHORT);
    }


   public static final boolean DEBUG_MODE=true;
}
