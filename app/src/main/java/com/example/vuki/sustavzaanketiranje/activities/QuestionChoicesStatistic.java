package com.example.vuki.sustavzaanketiranje.activities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 15.1.2016..
 */
public class QuestionChoicesStatistic implements Serializable {
    @SerializedName("question_choice_text")
    String answerText;

    @SerializedName("count")
    int count;

    @SerializedName("question_text_list")
    List<String> answerTexts;

    public String getAnswerText() {
        return answerText;
    }

    public int getCount() {
        return count;
    }
}
