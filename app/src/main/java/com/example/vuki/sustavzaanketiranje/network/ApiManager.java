package com.example.vuki.sustavzaanketiranje.network;

import com.example.vuki.sustavzaanketiranje.models.User;
import com.example.vuki.sustavzaanketiranje.network.deserializers.DateDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import java.util.Date;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Vuki on 10.11.2015..
 */
public class ApiManager implements ApiManagerInterface {
    private static final String TAG = "Network";
    public static final String BASE_URL = "http://vukilab.com:8000";
    //public static final String BASE_URL = "http://192.168.1.5:8000";
    //public static final String BASE_URL = "http://10.129.81.140:8000";
    //public static final String BASE_URL = "http://192.168.1.4:8000";

    /// not in use
  /*  public static Gson gson = new GsonBuilder()
            .registerTypeAdapter(UserAuthorized.class, new UserAuthorizedDeserializer())
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            .create();

    public static Gson getGson() {
        return gson;
    }
    */

    private static ApiManager instance;
    private ApiService service;


    public static Gson getGSON() {
        return GSON;
    }

    private static final Gson GSON = new GsonBuilder()
            .registerTypeAdapter(Date.class, new DateDeserializer())
            .create();


    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public synchronized static ApiManager getInstance() {
        if (instance == null) {
            instance = new ApiManager();
        }
        return instance;
    }

    private ApiManager() {
        OkHttpClient client = new OkHttpClient();
        client.interceptors().add(new ReceivedCookiesInterceptor());
        client.interceptors().add(new AddHeaderInterceptor());
      //  client.interceptors().add(new LoggingInterceptor());


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(ApiManager.GSON))  //ApiManager.GSON)
                .build();
        service = retrofit.create(ApiService.class);
    }


    public ApiService getService() {
        return service;
    }
}
