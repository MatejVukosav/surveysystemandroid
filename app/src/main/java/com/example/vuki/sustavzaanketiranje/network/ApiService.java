package com.example.vuki.sustavzaanketiranje.network;

import com.example.vuki.sustavzaanketiranje.activities.ResponseMessage;
import com.example.vuki.sustavzaanketiranje.models.FilledSurveyRequest;
import com.example.vuki.sustavzaanketiranje.models.LoginRequest;
import com.example.vuki.sustavzaanketiranje.models.LoginResponse;
import com.example.vuki.sustavzaanketiranje.models.RegisterRequest;
import com.example.vuki.sustavzaanketiranje.models.ResetPasswordRequest;
import com.example.vuki.sustavzaanketiranje.models.ResetPasswordResponse;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.SurveyCreateResponse;
import com.example.vuki.sustavzaanketiranje.models.SurveyIdRequest;
import com.example.vuki.sustavzaanketiranje.models.SurveyQuestionResponse;
import com.example.vuki.sustavzaanketiranje.models.SurveyStatistic;
import com.example.vuki.sustavzaanketiranje.models.SurveysResponse;
import com.example.vuki.sustavzaanketiranje.models.UpdateSurveyResponse;
import com.example.vuki.sustavzaanketiranje.models.UserId;
import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Vuki on 10.11.2015..
 */
public interface ApiService {


    @POST("/register/")
    Call<LoginResponse> postRegister(@Body RegisterRequest registerRequest);

    @POST("/login/")
    Call<LoginResponse> postLogin(@Body LoginRequest loginRequest);

    @POST("/create_survey/")
    Call<SurveyCreateResponse> postCreateSurvey(@Body Survey surveyRequest);

    //get all surveys
    @GET("/all_surveys/")
    Call<SurveysResponse> getAllSurveys();

    //get all surveys
    @GET("/surveys_for_fill/{user_id}")
    Call<SurveysResponse> getSurveysForFill(@Path("user_id")int userId);

    //get surveys from single author
    @POST("/list_surveys/")
    Call<SurveysResponse> getMySurveys(@Body UserId userId);

    @POST("/get_filled_surveys/")
    Call<SurveysResponse> getFilledSurveys(@Body UserId userId);

    //get questions from single survey
    @POST("/get_survey_questions/")
    Call<SurveyQuestionResponse> getSurveyQuestion(@Body SurveyIdRequest surveyId);

    @POST("/save_filled_survey/")
    Call<Void> saveFilledSurvey(@Body FilledSurveyRequest survey);

    @POST("/update_survey/")
    Call<UpdateSurveyResponse> updateSurvey(@Body Survey survey);

    @POST("/delete_survey/{id}")
    Call<ResponseMessage> deleteSurvey(@Path("id") int surveyId);

    String mobileDownloadSurveyStatisticUrl="/mobile/download/survey/answers/csv/";
    @GET(mobileDownloadSurveyStatisticUrl+"{survey_id}")
    Call<ResponseBody> downloadSurveyAnswersCsv(@Path("survey_id") int surveyId);


    String downloadSurveyStatisticUrl="/mobile/download/survey/statistic/";
    @GET(downloadSurveyStatisticUrl+"{survey_id}")
    Call<SurveyStatistic> downloadSurveyStatistic1(@Path("survey_id") int surveyId);


    @POST("/user/mobile/password/reset/")
    Call<ResetPasswordResponse> postResetPassword(@Body ResetPasswordRequest resetPasswordRequest);
    String webPasswordResetUrl = "/user/password/reset/";


}
