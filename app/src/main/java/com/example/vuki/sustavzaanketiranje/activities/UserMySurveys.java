package com.example.vuki.sustavzaanketiranje.activities;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.vuki.sustavzaanketiranje.DividerItemDecoration;
import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.adapters.AvailableSurveysAdapter;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyClickListener;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyDownloadListener;
import com.example.vuki.sustavzaanketiranje.models.Statistic;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.SurveyIdRequest;
import com.example.vuki.sustavzaanketiranje.models.SurveyQuestionResponse;
import com.example.vuki.sustavzaanketiranje.models.SurveyStatistic;
import com.example.vuki.sustavzaanketiranje.models.SurveysResponse;
import com.example.vuki.sustavzaanketiranje.models.UserId;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;
import com.example.vuki.sustavzaanketiranje.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class UserMySurveys<T> extends AppCompatActivity implements OnSurveyClickListener, OnSurveyDownloadListener {

    @Bind(R.id.my_survey_surveys_recycler_view)
    RecyclerView mRecyclerView;


    @OnClick(R.id.survey_opened)
    public void onOpenedClick() {
        NotesHelpers.toastMessage(context, "Survey is opened");
    }


    @OnClick(R.id.survey_editable)
    public void onEditableClick() {
        NotesHelpers.toastMessage(context, "Survey is editable");
    }

    @OnClick(R.id.survey_closed)
    public void onClosedClick() {
        NotesHelpers.toastMessage(context, "Survey is closed");
    }

    @OnClick(R.id.survey_not_opened_yet)
    public void onNotOpenedYetClick() {
        NotesHelpers.toastMessage(context, "Survey is not opened yet");
    }

    public static List<Survey> data;
    static Context context;

    private static BroadcastReceiver receiverDownloadComplete;
    private static BroadcastReceiver receiverNotificationClicked;
    private static long myDownloadReference;
    private static DownloadManager downloadManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_surveys);
        ButterKnife.bind(this);

        init();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("My surveys");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.survey_opened:
                Toast.makeText(this, "Survey is opened", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.survey_editable:
                Toast.makeText(UserMySurveys.this, "Survey is editable", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.survey_closed:
                Toast.makeText(UserMySurveys.this, "Survey is closed", Toast.LENGTH_SHORT).show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void init() {
        context = this;
        apiCall();
    }

    private void apiCall() {
        int userId = ApiManager.getInstance().getUser().getUserId();
        UserId userSurveysRequest = new UserId(userId);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        Utils.setProgressDialog(progressDialog);

        Call<SurveysResponse> mySurveysResponseCall = ApiManager.getInstance().getService().getMySurveys(userSurveysRequest);
        mySurveysResponseCall.enqueue(new Callback<SurveysResponse>() {
            @Override
            public void onResponse(Response<SurveysResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body().getSurveys() != null) {
                        try {
                            fillData(response.body().getSurveys());
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }

                        AvailableSurveysAdapter adapter = new AvailableSurveysAdapter(context, data, (OnSurveyClickListener) context, (OnSurveyDownloadListener) context, true, true, true);
                        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                        llm.setOrientation(LinearLayoutManager.VERTICAL);
                        mRecyclerView.setLayoutManager(llm);

                        mRecyclerView.setHasFixedSize(true);
                        mRecyclerView.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getApplicationContext(), R.drawable.abc_list_divider_mtrl_alpha)));
                        mRecyclerView.setAdapter(adapter);
                    }
                } else {
                    NotesHelpers.toastMessage(getApplicationContext(), "Error: response body is null");
                }

                Utils.dismissProgressDialog(progressDialog);
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(progressDialog);
                NotesHelpers.toastMessage(getApplicationContext(), "failed: " + t.getMessage());
                fillData(new ArrayList<Survey>());
            }
        });
    }

    private void fillData(List<Survey> surveys) {
        data = new ArrayList();
        for (Survey survey : surveys)
            data.add(survey);
    }

    @Override
    public void onRecyclerItemClickListener(Survey survey) {
        getQuestionsForSurvey(survey);
    }

    @Override
    public void onItemRemoved(int surveyPosition) {
        mRecyclerView.getAdapter().notifyItemRemoved(surveyPosition);
    }

    @Override
    public void onItemUpdated(int surveyId) {
        mRecyclerView.getAdapter().notifyItemChanged(surveyId);

    }


    private void getQuestionsForSurvey(final Survey survey) {
        int id = survey.getSurveyID();

        SurveyIdRequest surveyIdRequest = new SurveyIdRequest(id);
        Call<SurveyQuestionResponse> surveyQuestionResponseCall = ApiManager.getInstance().getService().getSurveyQuestion(surveyIdRequest);
        surveyQuestionResponseCall.enqueue(new Callback<SurveyQuestionResponse>() {
            @Override
            public void onResponse(Response<SurveyQuestionResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getSurveyQuestions() != null) {
            /*            NotesHelpers.toastMessage(context, "Kliknuo si na anketu: " + survey.getName());*/
                        survey.setQuestions(response.body().getSurveyQuestions());
                        survey.setAuthor(response.body().getSurveyAuthor());


                        Bundle b = new Bundle();
                        b.putBoolean(CreateNewSurveyActivity.IS_MY_SURVEY, true);
                        //if true closed is before now
                        if (Utils.dateBeforeNow(survey.getOpenedAt())) {
                            b.putBoolean(CreateNewSurveyActivity.MARK_EDIT_RIGHTS, false);
                            b.putBoolean(CreateNewSurveyActivity.MARK_READ_ONLY_RIGHTS, true);
                        } else {
                            b.putBoolean(CreateNewSurveyActivity.MARK_EDIT_RIGHTS, true);
                            b.putBoolean(CreateNewSurveyActivity.MARK_READ_ONLY_RIGHTS, false);
                        }
                        b.putBoolean(CreateNewSurveyActivity.MARK_FILL_RIGHTS, false);
                        b.putSerializable(CreateNewSurveyActivity.MARK_SURVEY, survey);

                        Intent surveyDetails = new Intent(UserMySurveys.this, CreateNewSurveyActivity.class);
                        surveyDetails.putExtras(b);
                        startActivity(surveyDetails);

                    } else if (response.body().getSurveyName() != null) {
                        NotesHelpers.toastMessage(context, "Survey " + response.body().getSurveyName() + " has no questions!");
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure: " + t.getMessage());
            }
        });

    }


    @Override
    public void onSurveyAnswersCsv(int surveyId, String surveyName) {

        String fileName = "SZA_" + surveyName + ".csv";
        String downloadUrl = ApiManager.BASE_URL + ApiService.mobileDownloadSurveyStatisticUrl + surveyId;
        Uri uri = Uri
                .parse(downloadUrl);

        String servicestring = Context.DOWNLOAD_SERVICE;
        downloadManager = (DownloadManager) context.getSystemService(servicestring);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDescription(surveyName + ".csv")
                .setTitle("Survey " + surveyName + " download")
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        //make file visible by and manageable by system's download app
                .setVisibleInDownloadsUi(true)
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName)
                .allowScanningByMediaScanner();
        myDownloadReference = downloadManager.enqueue(request);


    }

    @Override
    public void onSurveyDownload(int surveyId, String surveyName) {
        Call<SurveyStatistic> surveyStatisticCall = ApiManager.getInstance().getService().downloadSurveyStatistic1(surveyId);
        surveyStatisticCall.enqueue(new Callback<SurveyStatistic>() {
            @Override
            public void onResponse(Response<SurveyStatistic> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getStatistic() != null) {
                        Statistic surveyStatistic = response.body().getStatistic();

                        Bundle b = new Bundle();
                        b.putSerializable(StatisticActivity.STATISTIC, surveyStatistic);
                        Intent showStatistic = new Intent(UserMySurveys.this, StatisticActivity.class);
                        showStatistic.putExtras(b);
                        startActivity(showStatistic);

                    } else {
                        NotesHelpers.toastMessage(getApplicationContext(), "Error: response body is empty");

                    }
                } else {
                    NotesHelpers.toastMessage(getApplicationContext(), "Oops,something wen't wrong. Our support-team is notified.");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(getApplicationContext(), "failed " + t.getMessage());
                t.printStackTrace();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (mRecyclerView.getAdapter() != null) {
            apiCall();
            mRecyclerView.getAdapter().notifyDataSetChanged();
        }

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);
        receiverNotificationClicked = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                long[] references = intent.getLongArrayExtra(extraId);
                for (long reference : references) {
                    if (reference == myDownloadReference) {

                    }
                }
            }
        };
        registerReceiver(receiverNotificationClicked, filter);

        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        receiverDownloadComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (myDownloadReference == reference) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(reference);
                    Cursor cursor = downloadManager.query(query);
                    cursor.moveToFirst();
                    //get the status of the download
                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    int status = cursor.getInt(columnIndex);
                    int filenameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);
                    String savedFilePath = cursor.getString(filenameIndex);
                    //get the reason - more detail on the status  of the download
                    int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
                    int reason = cursor.getInt(columnReason);
                 /*   pending – 1
                    running – 2
                    paused - 4
                    success – 8
                    failed – 16*/
                    switch (status) {
                        case DownloadManager.STATUS_SUCCESSFUL:
                            // NotesHelpers.toastMessage(context,"SUCCESSFULL "+reason);
                            break;
                        case DownloadManager.STATUS_FAILED:
                            //  NotesHelpers.toastMessage(context,"FAILED "+reason);
                            break;
                        case DownloadManager.STATUS_PAUSED:
                            //   NotesHelpers.toastMessage(context,"PAUSED "+reason);
                            break;
                        case DownloadManager.STATUS_PENDING:
                            //    NotesHelpers.toastMessage(context,"PENDING "+reason);
                            break;
                        case DownloadManager.STATUS_RUNNING:
                            //      NotesHelpers.toastMessage(context,"RUNNING "+reason);
                            break;

                    }
                }
            }
        };
        registerReceiver(receiverDownloadComplete, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverNotificationClicked);
        unregisterReceiver(receiverDownloadComplete);
    }
}
