package com.example.vuki.sustavzaanketiranje.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.vuki.sustavzaanketiranje.EditQuestionInterface;
import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.adapters.CreateSurveyAdapter;
import com.example.vuki.sustavzaanketiranje.adapters.SingleQuestionAdapter;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyAnswer;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyQuestion;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditTextOnlyActivity extends AppCompatActivity implements EditQuestionInterface {


    @Bind(R.id.create_question_recycler_view)
    RecyclerView recyclerView;

    @Bind(R.id.question_name)
    EditText questionName;

    @Bind(R.id.create_question_edit_img_btn)
    ImageButton editQuestionBtn;

    String title;

    @Bind(R.id.create_survey_header_add)
    ImageView addAnswer;

    TextOnlyQuestion question;
    int questionPositionInRecycler;
    boolean createNewQuestion;
    boolean EDIT_MODE;
    boolean FILL_MODE;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_survey_questions_list);
        ButterKnife.bind(this);


        EDIT_MODE=CreateNewSurveyActivity.EDIT_RIGHTS;
        FILL_MODE=true;

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Bundle b = getIntent().getExtras();
        if (b != null) {
            question = (TextOnlyQuestion) getIntent().getExtras().getSerializable(String.valueOf(CreateSurveyAdapter.TEXT_ONY));
            questionPositionInRecycler = b.getInt(CreateSurveyAdapter.questionPositionInRecycler);
            SingleQuestionAdapter sqa = new SingleQuestionAdapter(question.answers, this, R.layout.create_text_only, FILL_MODE,EDIT_MODE, null);
            recyclerView.setAdapter(sqa);
            questionName.setText(question.questionText);
            questionName.setFocusable(true);
            title = "Edit question";
            createNewQuestion=false;
        } else {
            question=new TextOnlyQuestion();
            question.answers = new ArrayList<>();
            question.answers.add(new TextOnlyAnswer(""));
            questionName.setText("");
            SingleQuestionAdapter sqa = new SingleQuestionAdapter(question.answers, this, R.layout.create_text_only, FILL_MODE,EDIT_MODE,null);

            recyclerView.setAdapter(sqa);
            questionName.setText(question.questionText);
            questionName.setFocusable(true);
            questionPositionInRecycler = CreateNewSurveyActivity.mSurvey.getQuestions().size();
            title = "Create new question";
            createNewQuestion=true;
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(title);

        }

        addAnswer.setVisibility(View.GONE);
        editQuestionBtn.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NotesHelpers.toastMessage(this, "All changes are lost!");
                finish();
                return true;
            case R.id.edit_question_save:
                saveChanges();
                NotesHelpers.toastMessage(getApplicationContext(), "save changes");
                return true;
            case R.id.edit_question_delete:
                if (createNewQuestion) {
                    NotesHelpers.toastMessage(this, "All changes are lost!");
                    finish();
                } else {
                    deleteQuestion();
                    NotesHelpers.toastMessage(getApplicationContext(), "delete question");
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void addAnswer() {
        //no available answers
    }

    @Override
    public void saveChanges() {
        question.questionText = questionName.getText().toString();
        if (createNewQuestion) {
            CreateNewSurveyActivity.mSurvey.getQuestions().add(question);
        } else {
            CreateNewSurveyActivity.mSurvey.getQuestions().set(questionPositionInRecycler,question);
        }
        finish();
    }

    @Override
    public void deleteQuestion() {
        CreateNewSurveyActivity.mSurvey.getQuestions().remove(questionPositionInRecycler);
        finish();
    }

    @Override
    public void deleteAnswer(int position) {
        //no available answers

    }

}
