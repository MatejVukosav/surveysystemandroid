package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 3.12.2015..
 */
public class LoginRequest implements Serializable {


    @SerializedName("token")
    private String token;

    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;


    public LoginRequest(String username, String password) {
        this.password = password;
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
