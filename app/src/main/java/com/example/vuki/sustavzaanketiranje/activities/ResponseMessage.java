package com.example.vuki.sustavzaanketiranje.activities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 30.12.2015..
 */
public class ResponseMessage implements Serializable {

    @SerializedName("error")
    String error;

    @SerializedName("response")
    String responseMessage;

    public String getError() {
        return error;
    }

    public String getResponseMessage() {
        return responseMessage;
    }
}
