package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 29.12.2015..
 */
public class SurveysFromSingleUserRequest implements Serializable {

    @SerializedName("username")
    String username;

    public SurveysFromSingleUserRequest(String username) {
        this.username = username;
    }
}
