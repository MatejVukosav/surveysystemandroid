package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 18.11.2015..
 */
public class Survey<T> implements Serializable {

    @SerializedName("id")
    int surveyID;

    @SerializedName("name")
    String name;
    @SerializedName("opened_at")
    String openedAt;
    @SerializedName("closed_at")
    String closedAt;
    @SerializedName("author")
    String author;

    @SerializedName("author_id")
    int authorId;
    @SerializedName("description")
    String description;
    @SerializedName("questions")
    List<T> questions;


    public Survey(String name, String openedAt, String closedAt, String author, String description, List<T> questions) {
        this.name = name;
        this.openedAt = openedAt;
        this.closedAt = closedAt;
        this.author = author;
        this.description = description;
        this.questions = questions;
    }


    public List<T> getQuestions() {
        return questions;
    }

    public void setQuestions(List<T> questions) {
        this.questions = questions;
    }

    public String getName() {
        return name;
    }


    public String getOpenedAt() {
        return openedAt;
    }

    public String getClosedAt() {
        return closedAt;
    }


    public String getAuthor() {
        return author;
    }

    public String getDescription() {
        return description;
    }


    public void setOpenedAt(String openedAt) {
        this.openedAt = openedAt;
    }

    public void setClosedAt(String closedAt) {
        this.closedAt = closedAt;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSurveyID() {
        return surveyID;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }
}
