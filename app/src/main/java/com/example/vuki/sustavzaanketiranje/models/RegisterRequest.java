package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 10.11.2015..
 */
public class RegisterRequest implements Serializable {

    @SerializedName("username")
    String username;
    @SerializedName("email")
    String email;

    @SerializedName("password1")
    String password1;

    @SerializedName("password2")
    String password2;


    @SerializedName("first_name")
    String firstname;

    @SerializedName("last_name")
    String lastName;

    @SerializedName("age")
    int age;

    @SerializedName("genre")
    String genre;

    @SerializedName("city")
    String city;


    public RegisterRequest() {
    }

    public RegisterRequest(String username, String email, String password1, String password2, String firstname, String lastName, int age, String genre, String city) {
        this.username = username;
        this.email = email;
        this.password1 = password1;
        this.password2 = password2;
        this.firstname = firstname;
        this.lastName = lastName;
        this.age = age;
        this.genre = genre;
        this.city = city;
    }
}
