package com.example.vuki.sustavzaanketiranje;

/**
 * Created by Vuki on 15.12.2015..
 */
public interface EditQuestionInterface {

    void deleteQuestion();
    void deleteAnswer(int position);
    void addAnswer();
    void saveChanges();

}
