package com.example.vuki.sustavzaanketiranje.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.SendEmailHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/*
    NOT IN USE
 */
public class AskForHelpActivity extends AppCompatActivity {

    @Bind(R.id.ask_for_help_edit_text)
    EditText helpText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_for_help);
        ButterKnife.bind(this);

    }


    @OnClick(R.id.ask_for_help_send)
    public void sendEmail() {

        String helpMessage = helpText.getText().toString();
        String messageTitle = "Ask_for_help_SZA";
        String receiver = "vuki146@gmail.com";
        SendEmailHelper.sendEmail(this, receiver, messageTitle, helpMessage);

    }


}
