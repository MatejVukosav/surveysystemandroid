package com.example.vuki.sustavzaanketiranje.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.PrefsHelper;
import com.example.vuki.sustavzaanketiranje.helpers.SharedPrefsHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.models.LoginRequest;
import com.example.vuki.sustavzaanketiranje.models.LoginResponse;
import com.example.vuki.sustavzaanketiranje.models.User;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.login_username)
    EditText mUsername;
    @Bind(R.id.login_password)
    EditText mPassword;
    PrefsHelper prefsHelper;

    @Bind(R.id.home)
    Button home;

    User user;
    boolean mDoubleBackToExitPressedOnce = false;
    boolean mLoginSave = false;

    @OnClick(R.id.home)
    public void home(){
        User user=new User();
        user.setUsername("vuki");
        user.setEmail("email@email.com");
        ApiManager.getInstance().setUser(user);
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        prefsHelper = new PrefsHelper(this);


        String json = prefsHelper.getString(PrefsHelper.LOGGED_IN_USER_APPUSER_DATA, "");
        String password = prefsHelper.getString(PrefsHelper.LOGGED_IN_USER_APPUSER_DATA_PASSWORD, "");
        mLoginSave = prefsHelper.getBoolean(PrefsHelper.REMEMBER_USER_LOGIN_SKIP, false);


        if (!json.isEmpty() && json != null) {
            User user = ApiManager.getGSON().fromJson(json, User.class);
            if (user != null) {
                ApiManager.getInstance().setUser(user);

               SharedPrefsHelpers.setToken(this,user.getToken());

                mUsername.setText(user.getUsername()); //SET LAST LOGIN MAIL

                if (mLoginSave) {
                    if (password != null & !password.isEmpty()) {
                        mPassword.setText(password);
                    }
                }
            }

        }


    }

    @OnClick(R.id.login_btn)
    public void OnLoginClick() {

        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();
        LoginRequest loginRequest = new LoginRequest(username, password);
        prefsHelper.putBoolean(PrefsHelper.REMEMBER_USER_LOGIN_SKIP, false);

        final ProgressDialog progressDialog=new ProgressDialog(this);
        Utils.setProgressDialog(progressDialog);

        Call<LoginResponse> loginRequestCall = ApiManager.getInstance().getService().postLogin(loginRequest);
        loginRequestCall.enqueue(new Callback<LoginResponse>() {

            @Override
            public void onResponse(Response<LoginResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getUser() != null) {
                        user = response.body().getUser();
                        ApiManager.getInstance().setUser(user);

                        String json = ApiManager.getGSON().toJson(user);
                        prefsHelper.putString(PrefsHelper.LOGGED_IN_USER_APPUSER_DATA, json);

                        prefsHelper.putBoolean(PrefsHelper.REMEMBER_USER_LOGIN_SKIP, true);

                        Utils.dismissProgressDialog(progressDialog);

                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else if (response.body().getMessageError() != null) {
                        String message = response.body().getMessageError();
                        NotesHelpers.toastMessage(getApplicationContext(), message);

                    } else {
                        NotesHelpers.toastMessage(getApplicationContext(), "Oops,something wen't wrong. Server internal error");
                    }
                } else {
                    NotesHelpers.toastMessage(getApplicationContext(), "Oops,something wen't wrong. Our support-team is notified.");
                }
                Utils.dismissProgressDialog(progressDialog);
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(progressDialog);
                NotesHelpers.toastMessage(getApplicationContext(), "failed " + t.getMessage());

            }
        });


    }

    @OnClick(R.id.login_register_txt)
    public void onRegisterTextClick() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.login_retrieve_password)
    public void forgottenPassword() {
        Intent intent = new Intent(LoginActivity.this, ForgottenPasswordWebView.class);
        startActivity(intent);
    }

/*    @OnClick(R.id.builder)
    public void showBuilderActivity() {
        Intent intent = new Intent(LoginActivity.this, CreateNewSurveyActivity.class);
        READ_ONLY_RIGHTS = false;
        FILL_RIGHTS = false;
        EDIT_RIGHTS = true;

    }

    @OnClick(R.id.builderRead)
    public void showBuilderReadActivity() {
        Intent intent = new Intent(LoginActivity.this, CreateNewSurveyActivity.class);
        READ_ONLY_RIGHTS = true;
        FILL_RIGHTS = false;
        EDIT_RIGHTS = false;
        startActivity(intent);
    }

    @OnClick(R.id.builderFill)
    public void showBuilderFillActivity() {
        Intent intent = new Intent(LoginActivity.this, CreateNewSurveyActivity.class);
        READ_ONLY_RIGHTS = false;
        FILL_RIGHTS = true;
        EDIT_RIGHTS = false;
        startActivity(intent);
    }*/

    static boolean READ_ONLY_RIGHTS; //if survey has closed
    static boolean FILL_RIGHTS; //of survey is open
    static boolean EDIT_RIGHTS; //admin mode


    @Override
    public void onBackPressed() {
        if (mDoubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        mDoubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        //after 2seconds it becomes false
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDoubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
