package com.example.vuki.sustavzaanketiranje.network;

import android.text.TextUtils;

import com.example.vuki.sustavzaanketiranje.App;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.SharedPrefsHelpers;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.HashSet;

/**
 * Created by Vuki on 29.12.2015..
 */
public class AddHeaderInterceptor implements com.squareup.okhttp.Interceptor {

    private static final String TOKEN = "token";
    private static String TAG;

    @Override
    public Response intercept(Chain chain) throws IOException {

        TAG = getClass().getSimpleName();
        HashSet<String> cookies = new HashSet<>();
        String authToken = "";

        authToken = SharedPrefsHelpers.getToken(App.getInstance());
        String connectSid = SharedPrefsHelpers.getCookiesConnectSid(App.getInstance());

        if (connectSid != null && !connectSid.isEmpty()) {
            NotesHelpers.logMessage(TAG, "cookies: ");
            int i = 0;
            for (String cookie : cookies) {
                NotesHelpers.logMessage("" + i, cookie);
                i++;
            }
        } else {
            NotesHelpers.logMessage(TAG, "cookies are null");
        }
        if (authToken != null) {
            NotesHelpers.logMessage(TOKEN, authToken);
        }
        Request original = chain.request();
        if (!TextUtils.isEmpty(connectSid)) {
            Request request = original.newBuilder()
                    .header("user-agent", "SzaAndroid")
                    .header("Accept", "application/vnd.yourapi.v1.full+json")
                    .header("Authorization", authToken)
                    .addHeader("cookie", connectSid)
                    .method(original.method(), original.body())
                    .build();


            return chain.proceed(request);
        } else {
            NotesHelpers.logMessage(TAG, "token is empty");
        }

        return chain.proceed(original); //or null?
    }
}
