package com.example.vuki.sustavzaanketiranje.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.activities.ResponseMessage;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyClickListener;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyDownloadListener;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Vuki on 4.12.2015..
 */
public class AvailableSurveysAdapter extends RecyclerView.Adapter<AvailableSurveysAdapter.ViewHolder> {

    static List<Survey> data = new ArrayList();
    static Context context;
    static OnSurveyClickListener itemClickListener;
    static OnSurveyDownloadListener onSurveyDownloadListener;
    static String TAG;
    static boolean download;
    static boolean delete;
    boolean mySurvey;


    public AvailableSurveysAdapter(Context context, List<Survey> data, OnSurveyClickListener itemClickListener, OnSurveyDownloadListener onSurveyDownloadListener, boolean download, boolean delete, boolean mySurvey) {
        this.data = data;
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.onSurveyDownloadListener = onSurveyDownloadListener;
        TAG = getClass().toString();
        this.download = download;
        this.delete = delete;
        this.mySurvey = mySurvey;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.model_survey_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Survey currentSurvey = data.get(position);

        // true if closed is before now
        if (Utils.dateBeforeNow(currentSurvey.getClosedAt())) {
            holder.surveyPanel.setBackgroundColor(ContextCompat.getColor(context, R.color.colorModelClosed));
            // true if opened is before now
        } else if (!Utils.dateBeforeNow(currentSurvey.getOpenedAt()) && mySurvey) {
            holder.surveyPanel.setBackgroundColor(ContextCompat.getColor(context, R.color.colorModelEditable));
        } else if (Utils.dateBeforeNow(currentSurvey.getOpenedAt())) {
            holder.surveyPanel.setBackgroundColor(ContextCompat.getColor(context, R.color.colorModelOpen));
        } else {
            holder.surveyPanel.setBackgroundColor(ContextCompat.getColor(context, R.color.colorModelNotOpen));
        }

        holder.openetAt.setText(currentSurvey.getOpenedAt());
        holder.closedAt.setText(currentSurvey.getClosedAt());
        holder.surveyName.setText(currentSurvey.getName());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.model_survey_created_at_text)
        TextView openetAt;

        @Bind(R.id.model_survey_closed_at_text)
        TextView closedAt;

        @Bind(R.id.model_survey_name)
        TextView surveyName;

        @Bind(R.id.surveyPanel)
        LinearLayout surveyPanel;

        @Nullable
        @Bind(R.id.download_survey_answers_csv_img_view)
        ImageView downloadSurveyAnswersCsvImageView;
        @Nullable
        @Bind(R.id.download_survey_statistic_img_view)
        ImageView downloadSurveyStatisticImageView;
        @Nullable
        @Bind(R.id.delete_survey_img_view)
        ImageView deleteSurveyImageView;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

            if (downloadSurveyAnswersCsvImageView != null) {
                if (download) {
                    downloadSurveyAnswersCsvImageView.setVisibility(View.VISIBLE);
                    downloadSurveyAnswersCsvImageView.setOnClickListener(this);
                }
            }

            if (downloadSurveyStatisticImageView != null) {
                if (download) {
                    downloadSurveyStatisticImageView.setVisibility(View.VISIBLE);
                    downloadSurveyStatisticImageView.setOnClickListener(this);
                }
            }

            if (deleteSurveyImageView != null) {
                if (delete) {
                    deleteSurveyImageView.setVisibility(View.VISIBLE);
                    deleteSurveyImageView.setOnClickListener(this);
                }
            }
        }


        @Override
        public void onClick(View v) {
            Survey survey = data.get(getAdapterPosition());
            final int surveyId = survey.getSurveyID();
            String surveyName = survey.getName();
            switch (v.getId()) {
                case R.id.download_survey_answers_csv_img_view:
                    onSurveyDownloadListener.onSurveyAnswersCsv(surveyId, surveyName);
                    break;
                case R.id.delete_survey_img_view:
                    final Dialog dialog = Utils.showDeleteDialog(context, "Delete survey");
                    dialog.show();

                    Button btnOk = (Button) dialog.findViewById(R.id.dialog_ok);
                    btnOk.setOnClickListener(new View.OnClickListener(){
                         @Override
                         public void onClick(View v) {
                             onDeleteSurveyClick(surveyId, getAdapterPosition());
                             dialog.dismiss();
                         }
                     }
                    );

                    Button btnCancel = (Button) dialog.findViewById(R.id.dialog_cancel);
                    btnCancel.setOnClickListener(new View.OnClickListener(){
                         @Override
                         public void onClick(View v) {
                             dialog.dismiss();
                         }
                     }
                    );
                    break;
                case R.id.download_survey_statistic_img_view:
                    onSurveyDownloadListener.onSurveyDownload(surveyId, surveyName);
                    break;
                default:
                    Survey clickedSurvey = data.get(this.getLayoutPosition());
                    itemClickListener.onRecyclerItemClickListener(clickedSurvey);
                    break;
            }

        }

    }


    private static void onDeleteSurveyClick(final int surveyId, final int surveyPosition) {

        Call<ResponseMessage> deleteSurveyCall = ApiManager.getInstance().getService().deleteSurvey(surveyId);
        deleteSurveyCall.enqueue(new Callback<ResponseMessage>() {
            @Override
            public void onResponse(Response<ResponseMessage> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getResponseMessage() != null) {
                        NotesHelpers.toastMessage(context, "You succesfully deleted survey");
                        data.remove(surveyPosition);
                        itemClickListener.onItemRemoved(surveyPosition);

                    } else if (response.body().getError() != null) {
                        NotesHelpers.toastMessage(context, "Error: " + response.body().getError());
                    } else {
                        NotesHelpers.toastMessage(context, "Error: " + "response body is empty.");
                    }
                } else {
                    NotesHelpers.toastMessage(context, "Error: " + "response failed.");
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure: " + t.getMessage());
            }
        });
    }


   /* private static void onDownloadSurveyAnswersCsvClick(final String surveyName, int surveyId) {
        Call<ResponseBody> downloadSurveyCall = ApiManager.getInstance().getService().downloadSurveyAnswersCsv(surveyId);
        downloadSurveyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                String fileName = surveyName + " answers csv";
                try {
                    File path = Environment.getExternalStorageDirectory();
                    File file = new File(path, fileName);
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    IOUtils.write(response.body().bytes(), fileOutputStream);
                } catch (IOException e) {
                    NotesHelpers.toastMessage(context, "Error while writing file");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure: " + t.getMessage());
                t.printStackTrace();
            }
        });
    }*/


}
