package com.example.vuki.sustavzaanketiranje.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.listeners.OnAnswerClickListener;
import com.example.vuki.sustavzaanketiranje.models.MultipleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.SingleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyAnswer;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Vuki on 14.12.2015..
 */
public class SingleQuestionAdapter<T> extends RecyclerView.Adapter<SingleQuestionAdapter<T>.ViewHolder> {

    private List<T> mAnswers;
    private Context context;
    private int mLayout;
    boolean FILL_PERMISSION;
    boolean EDIT_PERMISSION;

    static OnAnswerClickListener onAnswerClickListener;


    public SingleQuestionAdapter(List<T> answers, Context context, int layout, boolean FILL_PERMISSION, boolean EDIT_PERMISSION, OnAnswerClickListener onAnswerClickListener) {
        this.mAnswers = answers;
        this.context = context;
        this.mLayout = layout;
        this.FILL_PERMISSION = FILL_PERMISSION;
        this.EDIT_PERMISSION = EDIT_PERMISSION;
        this.onAnswerClickListener = onAnswerClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(mLayout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Object object = mAnswers.get(position);

        if (object instanceof SingleChoiceAnswer) {
            final SingleChoiceAnswer sc = (SingleChoiceAnswer) object;
            holder.answer.setText(sc.getAnswer());

            if (FILL_PERMISSION && EDIT_PERMISSION) {
                holder.answer.setFocusable(true);
            } else {
                holder.answer.setFocusable(false);
            }

            if (EDIT_PERMISSION) {
                holder.deleteAnswer.setVisibility(View.VISIBLE);

                holder.answer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        sc.setAnswer(s.toString());
                    }
                });
            } else {
                holder.deleteAnswer.setVisibility(View.GONE);
            }

            if (sc.isChosen()) {
                holder.selectAnswerBox.setImageResource(android.R.drawable.radiobutton_on_background);
            } else {
                holder.selectAnswerBox.setImageResource(android.R.drawable.radiobutton_off_background);
            }


        } else if (object instanceof MultipleChoiceAnswer) {
            final MultipleChoiceAnswer mc = (MultipleChoiceAnswer) object;
            holder.answer.setText(mc.getAnswer());

            if (FILL_PERMISSION && EDIT_PERMISSION) {
                holder.answer.setFocusable(true);
            } else {
                holder.answer.setFocusable(false);
            }

            if (EDIT_PERMISSION) {

                holder.deleteAnswer.setVisibility(View.VISIBLE);

                holder.answer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        mc.setAnswer(s.toString());
                    }
                });
            } else {
                holder.deleteAnswer.setVisibility(View.GONE);
            }

            if (holder.selectAnswerBox != null) {
                if (mc.isChosen()) {
                    holder.selectAnswerBox.setImageResource(android.R.drawable.checkbox_on_background);
                } else {
                    holder.selectAnswerBox.setImageResource(android.R.drawable.checkbox_off_background);
                }
            }
        } else if (object instanceof TextOnlyAnswer) {
            final TextOnlyAnswer to = (TextOnlyAnswer) object;
            holder.answer.setText(to.getAnswer());

            if (FILL_PERMISSION) {
                holder.answer.setFocusable(true);
            } else {
                holder.answer.setFocusable(false);
            }


            holder.answer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    to.setAnswer(s.toString());
                }
            });
        }


    }


    @Override
    public int getItemCount() {
        return mAnswers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.create_single_question_text)
        EditText answer;
        @Nullable
        @Bind(R.id.create_single_question_image)
        ImageView selectAnswerBox;
        @Nullable
        @Bind(R.id.create_single_question_delete_answer)
        ImageView deleteAnswer;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

            if (deleteAnswer != null && EDIT_PERMISSION) {
                deleteAnswer.setOnClickListener(this);
            }

            if (selectAnswerBox != null && answer != null) {
                selectAnswerBox.setOnClickListener(this);
                answer.setOnClickListener(this);
            }

        }


        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            switch (v.getId()) {
                case R.id.create_single_question_delete_answer:
                    if (EDIT_PERMISSION) {
                        onAnswerClickListener.onItemDelete(position);
                    }
                    break;
                default:
                    if (FILL_PERMISSION && !EDIT_PERMISSION && !(mAnswers.get(0) instanceof TextOnlyAnswer)) {

                        if (mAnswers.get(position) instanceof SingleChoiceAnswer) {
                            for (Object o : mAnswers) {
                                SingleChoiceAnswer sca = (SingleChoiceAnswer) o;
                                if ((sca).equals(mAnswers.get(position))) {
                                    sca.setChosen(true);
                                } else {
                                    sca.setChosen(false);
                                }
                            }
                            notifyDataSetChanged();
                        } else {
                            MultipleChoiceAnswer mc = (MultipleChoiceAnswer) mAnswers.get(position);
                            if (mc.isChosen()) {
                                mc.setChosen(false);
                            } else {
                                mc.setChosen(true);
                            }
                        }
                        notifyItemChanged(position);
                    }
            }
        }
    }
}
