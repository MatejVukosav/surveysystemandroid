package com.example.vuki.sustavzaanketiranje.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.helpers.SendEmailHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/*
    NOT IN USE
 */
public class ReportBugActivity extends AppCompatActivity {

    @Bind(R.id.report_bug_edit_text)
    EditText bugText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_bug);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.report_bug_send)
    public void sendEmail() {

        String helpMessage = bugText.getText().toString();
        String messageTitle = "Bug_report_SZA";
        String receiver="vuki146@gmail.com";
        SendEmailHelper.sendEmail(this,receiver , messageTitle, helpMessage);

    }
}
