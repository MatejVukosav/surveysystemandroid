package com.example.vuki.sustavzaanketiranje.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.adapters.CreateSurveyAdapter;
import com.example.vuki.sustavzaanketiranje.models.MultipleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.MultipleChoiceQuestion;
import com.example.vuki.sustavzaanketiranje.models.Question;
import com.example.vuki.sustavzaanketiranje.models.SingleChoiceAnswer;
import com.example.vuki.sustavzaanketiranje.models.SingleChoiceQuestion;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyAnswer;
import com.example.vuki.sustavzaanketiranje.models.TextOnlyQuestion;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateNewSurveyActivity<T> extends AppCompatActivity  {


    @Bind(R.id.create_survey_recycler)
    RecyclerView recyclerView;

    public static Survey<Object> mSurvey;
    public static String TAG;

    @Bind(R.id.fab_menu)
    FloatingActionMenu fab_menu;

    @Bind(R.id.menu_item_single)
    FloatingActionButton fabSingle;
    @Bind(R.id.menu_item_multiple)
    FloatingActionButton fabMultiple;
    @Bind(R.id.menu_item_text_only)
    FloatingActionButton fabText;


    private String typeIdentifier = "questionTypeIdentificator";
    private String typeQuestiontext = "questionText";
    private String typeAnswers = "answers";

    private int typeSingleChoice = 0;
    private int typeMultipleChoice = 1;
    private int typeTextOnly = 2;

    static boolean READ_ONLY_RIGHTS; //
    static boolean FILL_RIGHTS; //
    static boolean EDIT_RIGHTS; //admin mode

    static String MARK_READ_ONLY_RIGHTS = "MARK_READ_ONLY_RIGHTS";
    static String MARK_FILL_RIGHTS = "MARK_FILL_RIGHTS";
    static String MARK_EDIT_RIGHTS = "MARK_EDIT_RIGHTS";
    static String MARK_SURVEY = "MARK_SURVEY";

    static String IS_MY_SURVEY = "IS_MY_SURVEY";
    static boolean IsMySurvey;

    public static String homeAdd = "from_home_to_create";
    CreateSurveyAdapter adapter;

    String surveyName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_survey);
        ButterKnife.bind(this);


        TAG = getClass().getSimpleName();


        init();
        fabClicks();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(surveyName);
        }

    }


    private void init() {


        Bundle b = getIntent().getExtras();
        Survey survey = null;
        if (b != null) {
            boolean createNewSurvey = b.getBoolean("CreateNewSurvey");
            if (!createNewSurvey) {
                IsMySurvey = b.getBoolean(IS_MY_SURVEY);
                Boolean value = b.getBoolean(MARK_EDIT_RIGHTS);
                if (value) {
                    EDIT_RIGHTS = true;
                    READ_ONLY_RIGHTS = false;
                    FILL_RIGHTS = false;
                } else {
                    value = b.getBoolean(MARK_FILL_RIGHTS);
                    if (value) {
                        EDIT_RIGHTS = false;
                        READ_ONLY_RIGHTS = false;
                        FILL_RIGHTS = true;
                    } else {
                        EDIT_RIGHTS = false;
                        READ_ONLY_RIGHTS = true;
                        FILL_RIGHTS = false;
                    }
                }
                survey = (Survey) b.getSerializable(MARK_SURVEY);
            } else {
                EDIT_RIGHTS = true;
                READ_ONLY_RIGHTS = false;
                FILL_RIGHTS = false;
                IsMySurvey = false;
            }
        }

        if (survey == null) {
            mSurvey = createEmptySurvey();
        } else {
            mSurvey = survey;
            if (!survey.getQuestions().isEmpty()) {
                //mSurvey.setQuestions(determineQuestionType(survey.getQuestions()));
                mSurvey.setQuestions(parseSurveyQuestions(survey.getQuestions()));
            }
        }

        surveyName = mSurvey.getName();
        adapter = new CreateSurveyAdapter(mSurvey, this, READ_ONLY_RIGHTS, EDIT_RIGHTS, FILL_RIGHTS, IsMySurvey);
        if (recyclerView.getLayoutManager() == null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        }

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        fab_menu.close(false);

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        adapter = new CreateSurveyAdapter(mSurvey, this, READ_ONLY_RIGHTS, EDIT_RIGHTS, FILL_RIGHTS, IsMySurvey);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private List<Object> parseSurveyQuestions(List<Object> q) {
        List<Object> questions = new ArrayList<>();

        for (Object q1 : q) {
            Question oneQuestion = (Question) q1;
            if ((oneQuestion).getQuestionTypeIdentificator() == typeSingleChoice) {
                List<SingleChoiceAnswer> scList = singleChoiceAnswerType(oneQuestion.getAnswers());
                SingleChoiceQuestion scq = new SingleChoiceQuestion(oneQuestion.getQuestionId(),oneQuestion.getQuestionText(), scList);
                questions.add(scq);
            } else if (oneQuestion.getQuestionTypeIdentificator() == typeMultipleChoice) {
                List<MultipleChoiceAnswer> mcList = multipleChoiceAnswerType(oneQuestion.getAnswers());
                MultipleChoiceQuestion mcq = new MultipleChoiceQuestion(oneQuestion.getQuestionId(),oneQuestion.getQuestionText(), mcList);
                questions.add(mcq);
            } else if (oneQuestion.getQuestionTypeIdentificator() == typeTextOnly) {
                List<TextOnlyAnswer> toaList = textOnlyAnswerType(oneQuestion.getAnswers());
                TextOnlyQuestion toq = new TextOnlyQuestion(oneQuestion.getQuestionId(),oneQuestion.getQuestionText(), toaList);
                questions.add(toq);
            }

        }


        return questions;
    }

    private List<Object> determineQuestionType(List<Object> q) {

        List<Object> questions = new ArrayList<>();
        Question question = new Question();

        for (int i = 0; i < q.size(); i++) {
            LinkedHashMap<String, T> obj = (LinkedHashMap<String, T>) q.get(i);

            for (Map.Entry<String, T> entry : obj.entrySet()) {
                String key = entry.getKey();
                T value = entry.getValue();

                if (key.equals(typeIdentifier)) {
                    if (value.equals(0)) {
                        question.setQuestionTypeIdentificator(Double.valueOf(value.toString()).intValue());
                    } else if (value.equals(1)) {
                        question.setQuestionTypeIdentificator(Double.valueOf(value.toString()).intValue());
                    } else {
                        question.setQuestionTypeIdentificator(Double.valueOf(value.toString()).intValue());
                    }
                } else if (key.equals(typeQuestiontext)) {
                    question.setQuestionText(value.toString());
                } else if (key.equals(typeAnswers)) {
                    question.setAnswers((List<? extends Object>) value);
                }
            }


            if (question.getQuestionTypeIdentificator() == 0) {
                SingleChoiceQuestion s = new SingleChoiceQuestion();
                s.questionText = question.getQuestionText();
                s.answers = singleChoiceAnswerType(question.getAnswers());
                s.questionTypeIdentificator = 0;
                questions.add(s);
            } else if (question.getQuestionTypeIdentificator() == 1) {
                MultipleChoiceQuestion s = new MultipleChoiceQuestion();
                s.questionText = question.getQuestionText();
                s.answers = multipleChoiceAnswerType(question.getAnswers());
                s.questionTypeIdentificator = 1;
                questions.add(s);
            } else {
                TextOnlyQuestion s = new TextOnlyQuestion();
                s.questionText = question.getQuestionText();
                s.answers = textOnlyAnswerType(question.getAnswers());
                s.questionTypeIdentificator = 2;
                questions.add(s);
            }
        }

        return questions;
    }

    private List<SingleChoiceAnswer> singleChoiceAnswerType(List<Object> obj) {
        List<SingleChoiceAnswer> answerList = new ArrayList<>();
        SingleChoiceAnswer answer;
        //list of answers
        for (int j = 0; j < obj.size(); j++) {
            LinkedHashMap<String, T> ans = (LinkedHashMap<String, T>) obj.get(j);
            answer = new SingleChoiceAnswer();
            //for every answer data
            for (Map.Entry<String, T> entry : ans.entrySet()) {
                String key = entry.getKey();
                T value = entry.getValue();

                if (key.equals("text")) {
                    answer.setAnswer(value.toString());
                } else if (key.equals("id")) {
                    answer.setId(Double.valueOf(value.toString()).intValue());
                } else if (key.equals("chosen")) {
                    answer.setChosen(Boolean.valueOf(value.toString()));
                }
            }
            answerList.add(answer);
        }
        return answerList;
    }

    private List<MultipleChoiceAnswer> multipleChoiceAnswerType(List<Object> obj) {
        List<MultipleChoiceAnswer> answerList = new ArrayList<>();
        MultipleChoiceAnswer answer;
        //list of answers
        for (int j = 0; j < obj.size(); j++) {
            LinkedHashMap<String, T> ans = (LinkedHashMap<String, T>) obj.get(j);
            answer = new MultipleChoiceAnswer();
            //for every answer data
            for (Map.Entry<String, T> entry : ans.entrySet()) {
                String key = entry.getKey();
                T value = entry.getValue();

                if (key.equals("text")) {
                    answer.setAnswer(value.toString());
                } else if (key.equals("id")) {
                    answer.setId(Double.valueOf(value.toString()).intValue());
                } else if (key.equals("chosen")) {
                    answer.setChosen(Boolean.valueOf(value.toString()));
                }
            }
            answerList.add(answer);
        }
        return answerList;
    }

    private List<TextOnlyAnswer> textOnlyAnswerType(List<Object> obj) {
        List<TextOnlyAnswer> answerList = new ArrayList<>();
        TextOnlyAnswer answer;
        //list of answers
        for (int j = 0; j < obj.size(); j++) {
            LinkedHashMap<String, T> ans = (LinkedHashMap<String, T>) obj.get(j);
            answer = new TextOnlyAnswer();
            //for every answer data
            for (Map.Entry<String, T> entry : ans.entrySet()) {
                String key = entry.getKey();
                T value = entry.getValue();

                if (key.equals("text")) {
                    answer.setAnswer(value.toString());
                } else if (key.equals("id")) {
                    answer.setId(Double.valueOf(value.toString()).intValue());
                }
            }
            answerList.add(answer);
        }
        return answerList;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void fabClicks() {
        fab_menu.setClosedOnTouchOutside(true);
        if (EDIT_RIGHTS) {
            fab_menu.setVisibility(View.VISIBLE);
            fabSingle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CreateNewSurveyActivity.this, EditSingleChoiceActivity.class);
                    startActivity(intent);


                }
            });
            fabMultiple.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CreateNewSurveyActivity.this, EditMultipleChoiceActivity.class);
                    startActivity(intent);

                }
            });
            fabText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CreateNewSurveyActivity.this, EditTextOnlyActivity.class);
                    startActivity(intent);

                }
            });
        } else {
            fab_menu.setVisibility(View.GONE);
        }

    }



    private Survey createEmptySurvey() {
        String user;
        if (ApiManager.getInstance().getUser() != null) {
            user = ApiManager.getInstance().getUser().getUsername();
        } else {
            user = "user_error";
        }
        List<Object> questionses = new ArrayList<>();
        return new Survey("", "", "", user, "", questionses);
    }


    private void mockData() {
        List<Object> questionses = new ArrayList<>();
        SingleChoiceQuestion q1 = new SingleChoiceQuestion();
        q1.questionText = "Koliko imas godina";
        for (int i = 18; i < 22; i++) {
            SingleChoiceAnswer sc1 = new SingleChoiceAnswer("" + i, false);
            q1.answers.add(sc1);
        }
        questionses.add(q1);

        MultipleChoiceQuestion q2 = new MultipleChoiceQuestion();
        q2.questionText = "Koja si godina faksa";
        for (int i = 1; i < 5; i++) {
            MultipleChoiceAnswer mc1 = new MultipleChoiceAnswer("" + i, false);
            q2.answers.add(mc1);
        }
        questionses.add(q2);

        TextOnlyQuestion q3 = new TextOnlyQuestion();
        q3.questionText = "Kako si?";
        TextOnlyAnswer to = new TextOnlyAnswer("");
        q3.answers.add(to);
        questionses.add(q3);
    }



}
