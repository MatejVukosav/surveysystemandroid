package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Vuki on 8.1.2016..
 */
public class ResetPasswordResponse implements Serializable {

    @SerializedName("error")
    String error;

    @SerializedName("response")
    String response;

    public String getError() {
        return error;
    }

    public String getResponse() {
        return response;
    }
}
