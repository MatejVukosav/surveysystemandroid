package com.example.vuki.sustavzaanketiranje.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.vuki.sustavzaanketiranje.DividerItemDecoration;
import com.example.vuki.sustavzaanketiranje.R;
import com.example.vuki.sustavzaanketiranje.adapters.AvailableSurveysAdapter;
import com.example.vuki.sustavzaanketiranje.helpers.NotesHelpers;
import com.example.vuki.sustavzaanketiranje.helpers.Utils;
import com.example.vuki.sustavzaanketiranje.listeners.OnSurveyClickListener;
import com.example.vuki.sustavzaanketiranje.models.Survey;
import com.example.vuki.sustavzaanketiranje.models.SurveyIdRequest;
import com.example.vuki.sustavzaanketiranje.models.SurveyQuestionResponse;
import com.example.vuki.sustavzaanketiranje.models.SurveysResponse;
import com.example.vuki.sustavzaanketiranje.models.UserId;
import com.example.vuki.sustavzaanketiranje.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class UserFilledSurveysActivity extends AppCompatActivity implements OnSurveyClickListener {

    @Bind(R.id.user_filled_surveys_recycler_view)
    RecyclerView mUserFilledSurveys;

    @OnClick(R.id.survey_opened)
    public void onOpenedClick() {
        NotesHelpers.toastMessage(context, "Survey is opened");
    }


    @OnClick(R.id.survey_editable)
    public void onEditableClick() {
        NotesHelpers.toastMessage(context, "Survey is editable");
    }

    @OnClick(R.id.survey_closed)
    public void onClosedClick() {
        NotesHelpers.toastMessage(context, "Survey is closed");
    }

    @OnClick(R.id.survey_not_opened_yet)
    public void onNotOpenedYetClick() {
        NotesHelpers.toastMessage(context, "Survey is not opened yet");
    }

    List<Survey> data = new ArrayList<>();
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_filled_surveys);
        ButterKnife.bind(this);

        init();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Filled surveys");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_return, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void init() {
        context = this;

        int userId = ApiManager.getInstance().getUser().getUserId();
        UserId filledSurveysRequest = new UserId(userId);


        final ProgressDialog progressDialog=new ProgressDialog(this);
        Utils.setProgressDialog(progressDialog);

        Call<SurveysResponse> surveysResponseCall = ApiManager.getInstance().getService().getFilledSurveys(filledSurveysRequest);
        surveysResponseCall.enqueue(new Callback<SurveysResponse>() {
            @Override
            public void onResponse(Response<SurveysResponse> response, Retrofit retrofit) {

                if (response.isSuccess()) {
                    if (response.body() != null) {
                        if (response.body().getSurveys() != null) {
                            fillData(response.body().getSurveys());

                            AvailableSurveysAdapter adapter = new AvailableSurveysAdapter(getApplicationContext(), data, (OnSurveyClickListener) context,null, false, false,false);
                            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
                            llm.setOrientation(LinearLayoutManager.VERTICAL);
                            mUserFilledSurveys.setLayoutManager(llm);

                            mUserFilledSurveys.setHasFixedSize(true);
                            mUserFilledSurveys.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(getApplicationContext(), R.drawable.abc_list_divider_mtrl_alpha)));
                            mUserFilledSurveys.setAdapter(adapter);
                        } else {
                            NotesHelpers.toastMessage(getApplicationContext(), "Error: there is no surveys");
                        }

                    } else {
                        NotesHelpers.toastMessage(getApplicationContext(), "Error: response body is null");
                    }
                }
                Utils.dismissProgressDialog(progressDialog);
            }

            @Override
            public void onFailure(Throwable t) {
                Utils.dismissProgressDialog(progressDialog);
                NotesHelpers.toastMessage(getApplicationContext(), "failed: " + t.getMessage());
                fillData(new ArrayList<Survey>());
            }
        });

    }

    private void fillData(List<Survey> surveys) {
        for (Survey survey : surveys)
            data.add(survey);
    }

    @Override
    public void onRecyclerItemClickListener(Survey survey) {
      /*  getQuestionsForSurvey(survey);*/
        NotesHelpers.toastMessage(this, "You have already filled this survey!");
    }

    @Override
    public void onItemRemoved(int surveyPosition) {
        mUserFilledSurveys.getAdapter().notifyItemRemoved(surveyPosition);
    }

    @Override
    public void onItemUpdated(int surveyId) {

    }

    private void getQuestionsForSurvey(final Survey survey) {
        int id = survey.getSurveyID();

        SurveyIdRequest surveyIdRequest = new SurveyIdRequest(id);
        Call<SurveyQuestionResponse> surveyQuestionResponseCall = ApiManager.getInstance().getService().getSurveyQuestion(surveyIdRequest);
        surveyQuestionResponseCall.enqueue(new Callback<SurveyQuestionResponse>() {
            @Override
            public void onResponse(Response<SurveyQuestionResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    if (response.body().getSurveyQuestions() != null) {
                        survey.setQuestions(response.body().getSurveyQuestions());
                        survey.setAuthor(response.body().getSurveyAuthor());

                        Bundle b = new Bundle();
                        b.putBoolean(CreateNewSurveyActivity.IS_MY_SURVEY,false);
                        b.putBoolean(CreateNewSurveyActivity.MARK_EDIT_RIGHTS, false);
                        b.putBoolean(CreateNewSurveyActivity.MARK_FILL_RIGHTS, false);
                        b.putBoolean(CreateNewSurveyActivity.MARK_READ_ONLY_RIGHTS, true);
                        b.putSerializable(CreateNewSurveyActivity.MARK_SURVEY, survey);

                        Intent surveyDetails = new Intent(UserFilledSurveysActivity.this, CreateNewSurveyActivity.class);
                        surveyDetails.putExtras(b);
                        startActivity(surveyDetails);

                    } else if (response.body().getSurveyName() != null) {
                        NotesHelpers.toastMessage(context, "Survey " + response.body().getSurveyName() + " has no questions!");
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                NotesHelpers.toastMessage(context, "Failure: " + t.getMessage());
            }
        });

    }


}
