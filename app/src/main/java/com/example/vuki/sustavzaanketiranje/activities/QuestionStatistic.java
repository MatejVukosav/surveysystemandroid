package com.example.vuki.sustavzaanketiranje.activities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Vuki on 15.1.2016..
 */
public class QuestionStatistic implements Serializable {

    @SerializedName("question_choice")
    List<QuestionChoicesStatistic> questionChoicesStatisticList;

    @SerializedName("question_text")
    String questionText;

    @SerializedName("question_type_identifikator")
    int questionTypeIdentificator;

    public List<QuestionChoicesStatistic> getQuestionChoicesStatisticList() {
        return questionChoicesStatisticList;
    }

    public String getQuestionText() {
        return questionText;
    }

    public int getQuestionTypeIdentificator() {
        return questionTypeIdentificator;
    }
}
