package com.example.vuki.sustavzaanketiranje.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vuki on 22.12.2015..
 */
public class Question<T> implements Serializable {

    @SerializedName("question_id")
    int questionId;

    @SerializedName("question_type_identificator")
     String questionTypeIdentificator;
    @SerializedName("question_text")
     String questionText;
    @SerializedName("answers")
     List<T> answers = new ArrayList<>();

    /*public void setQuestionTypeIdentificator(int questionTypeIdentificator) {
        this.questionTypeIdentificator = questionTypeIdentificator;
    }*/

    public int getQuestionTypeIdentificator() {
        return Integer.valueOf(questionTypeIdentificator);
    }

    public void setQuestionTypeIdentificator(int questionTypeIdentificator) {
        this.questionTypeIdentificator = String.valueOf(questionTypeIdentificator);
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public List<T> getAnswers() {
        return answers;
    }

    public void setAnswers(List<T> answers) {
        this.answers = answers;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public int getQuestionId() {
        return questionId;
    }
}

